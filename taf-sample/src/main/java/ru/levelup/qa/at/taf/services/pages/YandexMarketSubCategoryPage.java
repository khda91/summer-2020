package ru.levelup.qa.at.taf.services.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.ElementsCollection;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.$$;

public class YandexMarketSubCategoryPage extends AbstractYandexMarketPage {

    private ElementsCollection productList = $$("article[data-zone-name='snippet-card']");

    @FindBy(partialLinkText = "Сравнить")
    public SelenideElement compareButton;

    public void addProductToCompareByIndex(int productIndex) {
        productList.get(productIndex)
                .$x("div[2]/div[contains(@aria-label, 'Добавить')]")
                .hover()
                .shouldBe(Condition.visible)
                .click();
    }

    public String getProductNameByIndex(int productIndex) {
        return productList.get(productIndex)
                .$("[data-zone-name='title']")
                .getText().trim();
    }
}
