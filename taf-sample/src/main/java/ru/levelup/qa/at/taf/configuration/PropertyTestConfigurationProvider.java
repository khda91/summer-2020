package ru.levelup.qa.at.taf.configuration;

import org.apache.commons.lang3.StringUtils;

import java.io.IOException;
import java.util.Properties;

public class PropertyTestConfigurationProvider implements TestConfigurationProvider {

    private static final String PROPERTY_NAME = "conf";

    @Override
    public TestConfiguration loadConfiguration() {
        Properties properties = new Properties();
        String propertiesFileName = "conf/" + getConfValue() + ".properties";
        try {
            properties.load(this.getClass().getClassLoader().getResourceAsStream(propertiesFileName));
        } catch (IOException e) {
            e.printStackTrace();
        }

        return new TestConfiguration(properties.getProperty("web.site.url"), Long.parseLong(properties.getProperty("implicitly.wait.timeout.millis")));
    }

    private String getConfValue() {
        String confValue = System.getProperty(PROPERTY_NAME);
        if (StringUtils.isEmpty(confValue)) {
            confValue = "qa-env";
//            throw new IllegalArgumentException("Unable to find system property " + PROPERTY_NAME);
        }
        return confValue;
    }

}
