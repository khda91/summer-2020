package ru.levelup.qa.at.taf.services.pages;

import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.page;

public abstract class AbstractYandexMarketPage {

    protected AbstractYandexMarketPage() {
        page(this);
    }

    public void openCategory(String category) {
        clickToElementByText(category, "[data-zone-name='category-link']");
    }

    public void openSubCategory(String subCategory) {
        clickToElementByText(subCategory, "[data-zone-name='link']");
    }

    protected void clickToElementByText(String text, String cssLocator) {
        $$(cssLocator)
                .stream()
                .filter(se -> se.text().equalsIgnoreCase(text))
                .findFirst()
                .get()
                .click();
    }
}
