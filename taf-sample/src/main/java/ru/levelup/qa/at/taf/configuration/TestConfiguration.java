package ru.levelup.qa.at.taf.configuration;

public class TestConfiguration {

    private String url;

    private long implicitlyWaitTimeoutMillis;

    public TestConfiguration(String url, long implicitlyWaitTimeoutMillis) {
        this.url = url;
        this.implicitlyWaitTimeoutMillis = implicitlyWaitTimeoutMillis;
    }

    public String getUrl() {
        return url;
    }

    public long getImplicitlyWaitTimeoutMillis() {
        return implicitlyWaitTimeoutMillis;
    }
}
