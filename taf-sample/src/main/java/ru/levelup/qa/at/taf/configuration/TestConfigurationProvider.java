package ru.levelup.qa.at.taf.configuration;

public interface TestConfigurationProvider {

    TestConfiguration loadConfiguration();

}
