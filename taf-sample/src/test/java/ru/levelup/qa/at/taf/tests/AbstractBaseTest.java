package ru.levelup.qa.at.taf.tests;

import com.codeborne.selenide.Configuration;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import ru.levelup.qa.at.taf.configuration.PropertyTestConfigurationProvider;
import ru.levelup.qa.at.taf.configuration.TestConfiguration;
import ru.levelup.qa.at.taf.configuration.TestConfigurationProvider;
import ru.levelup.qa.at.taf.step.ActionStep;
import ru.levelup.qa.at.taf.step.AssertionStep;

import static com.codeborne.selenide.Selenide.closeWindow;
import static com.codeborne.selenide.Selenide.open;

public abstract class AbstractBaseTest {

    protected ActionStep actionStep;
    protected AssertionStep assertionStep;
    protected TestConfiguration configuration;

    @BeforeSuite
    public void loadConfiguration() {
        TestConfigurationProvider configurationProvider = new PropertyTestConfigurationProvider();
        configuration = configurationProvider.loadConfiguration();
        Configuration.timeout = configuration.getImplicitlyWaitTimeoutMillis();
    }

    @BeforeMethod
    public void setUp() {
        open(configuration.getUrl());

        actionStep = new ActionStep();
        assertionStep = new AssertionStep();
    }

    @AfterMethod
    public void tearDown() {
        closeWindow();
    }

}
