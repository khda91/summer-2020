package ru.levelup.qa.at.taf.tests;

import org.testng.annotations.Test;
import ru.levelup.qa.at.taf.dataprovider.YandexMarketDataProvider;

import java.util.ArrayList;
import java.util.List;

public class CompareProductsYandexMarketIT extends AbstractBaseTest {

    @Test(dataProviderClass = YandexMarketDataProvider.class, dataProvider = "Compare Products Data Provider")
    public void compareProductsTest(String category, String subCategory) {
//    @Test
//    public void compareProductsTest() {
        actionStep.openCategory(category);
        actionStep.openSubCategory(subCategory);
//        actionStep.openCategory("Компьютеры");
//        actionStep.openSubCategory("Ноутбуки");

        List<String> expectedItem = new ArrayList<>();
        String productName = actionStep.addProductToCompareListByIndex(0);
        expectedItem.add(productName);
        productName = actionStep.addProductToCompareListByIndex(1);
        expectedItem.add(productName);

        actionStep.clickToCompareButton();

        assertionStep.checkThatProductsAddedToCompareList(expectedItem);
    }

    @Test(dataProviderClass = YandexMarketDataProvider.class, dataProvider = "Compare Products Data Provider")
    public void compareProductsTest1(String category, String subCategory) {
//    @Test
//    public void compareProductsTest1() {
        actionStep.openCategory(category);
        actionStep.openSubCategory(subCategory);
//        actionStep.openCategory("Бытовая техника");
//        actionStep.openSubCategory("Плиты");

        List<String> expectedItem = new ArrayList<>();
        String productName = actionStep.addProductToCompareListByIndex(0);
        expectedItem.add(productName);
        productName = actionStep.addProductToCompareListByIndex(1);
        expectedItem.add(productName);

        actionStep.clickToCompareButton();

        assertionStep.checkThatProductsAddedToCompareList(expectedItem);
    }

}
