package ru.levelup.qa.at.taf.dataprovider;

import org.testng.annotations.DataProvider;

public class YandexMarketDataProvider {

    @DataProvider(name = "Compare Products Data Provider")
    public Object[][] compareProductsDataProvider() {
        return new Object[][]{
                {"Компьютеры", "Ноутбуки"},
                {"Электроника", "Мобильные телефоны"},
                {"Бытовая техника", "Плиты"}
        };
    }
}
