package ru.levelup.qa.at.taf.step;

import io.qameta.allure.Step;

public class ActionStep extends AbstractBaseStep {

    @Step("Открыть категорию '{0}'")
    public void openCategory(String category) {
        yandexMarketMainPage.openCategory(category);
    }

    @Step("Открыть подкатегорию '{0}'")
    public void openSubCategory(String subCategory) {
        yandexMarketCategoryPage.openSubCategory(subCategory);
    }

    @Step("Выбрать продукт с порядковым индексом '{0}'")
    public String addProductToCompareListByIndex(int productIndex) {
        yandexMarketSubCategoryPage.addProductToCompareByIndex(productIndex);
        return yandexMarketSubCategoryPage.getProductNameByIndex(productIndex);
    }

    @Step("Нажать на кнопку 'Сравнить'")
    public void clickToCompareButton() {
        yandexMarketSubCategoryPage.compareButton.click();
    }
}
