package ru.levelup.qa.at.taf.step;

import io.qameta.allure.Step;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

public class AssertionStep extends AbstractBaseStep {

    @Step("Продукты '{0}' должны отображиться на старинице сравнения")
    public void checkThatProductsAddedToCompareList(List<String> expectedProducts) {
        assertThat(yandexMarketComparePage.productList.texts())
                .containsExactlyInAnyOrderElementsOf(expectedProducts);
    }
}
