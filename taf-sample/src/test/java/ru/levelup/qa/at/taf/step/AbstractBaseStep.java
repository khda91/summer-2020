package ru.levelup.qa.at.taf.step;

import ru.levelup.qa.at.taf.services.pages.YandexMarketCategoryPage;
import ru.levelup.qa.at.taf.services.pages.YandexMarketComparePage;
import ru.levelup.qa.at.taf.services.pages.YandexMarketMainPage;
import ru.levelup.qa.at.taf.services.pages.YandexMarketSubCategoryPage;

public abstract class AbstractBaseStep {

    YandexMarketMainPage yandexMarketMainPage;
    YandexMarketCategoryPage yandexMarketCategoryPage;
    YandexMarketSubCategoryPage yandexMarketSubCategoryPage;
    YandexMarketComparePage yandexMarketComparePage;

    protected AbstractBaseStep() {
        yandexMarketMainPage = new YandexMarketMainPage();
        yandexMarketCategoryPage = new YandexMarketCategoryPage();
        yandexMarketSubCategoryPage = new YandexMarketSubCategoryPage();
        yandexMarketComparePage = new YandexMarketComparePage();
    }

}
