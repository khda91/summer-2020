package ru.levelup.qa.at.database;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.levelup.qa.at.database.model.PersonModel;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class DatabaseTest {

    private Connection connection;

    @BeforeMethod
    public void setUp() {
        connection = PostgresDatabaseConnector.getInstance().getConnection();
    }

    @Test
    public void getDataFromDataBase() {
        final String SELECT_SQL = "SELECT * FROM \"Person\"";
        Statement statement = null;

        try {
            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_SQL);
            while (resultSet.next()) {
                System.out.println(String.format("id = %d || first_name = %s || last_name = %s || email = %s",
                        resultSet.getInt(1),
                        resultSet.getString("first_name"),
                        resultSet.getString(3),
                        resultSet.getString("email")));
                System.out.println("===========================");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (statement != null && !statement.isClosed()) {
                    statement.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }
    }

    @Test
    public void insertDataFromDataBase() {
        List<PersonModel> personModels = new ArrayList<>();
        final String SELECT_SQL = "SELECT * FROM \"Person\"";
        final String INSERT_SQL = "INSERT INTO \"Person\" (first_name, last_name, email) VALUES (?, ?, ?)";

        PreparedStatement preparedStatement = null;
        Statement statement = null;
        try {
            preparedStatement = connection.prepareStatement(INSERT_SQL);
            preparedStatement.setString(1, "Jane");
            preparedStatement.setString(2, "Dou");
            preparedStatement.setString(3, "jane.dou@yandex.ru");
            preparedStatement.execute();

            statement = connection.createStatement();
            ResultSet resultSet = statement.executeQuery(SELECT_SQL);
            while (resultSet.next()) {
                PersonModel personModel = new PersonModel(resultSet.getInt(1),
                        resultSet.getString("first_name"),
                        resultSet.getString(3),
                        resultSet.getString("email"));
                personModels.add(personModel);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            try {
                if (preparedStatement != null && !preparedStatement.isClosed()) {
                    preparedStatement.close();
                }

                if (statement != null && !statement.isClosed()) {
                    statement.close();
                }
            } catch (SQLException throwables) {
                throwables.printStackTrace();
            }
        }

        System.out.println(personModels);
    }

    @AfterMethod
    public void tearDown() {
        try {
            if (connection != null && !connection.isClosed()) {
                connection.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
