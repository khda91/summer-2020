package ru.levelup.qa.at.selenium.scripts;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class GmailTest {

    @Test
    public void openGmailTest1() {
        System.setProperty("webdriver.chrome.driver", this.getClass()
                .getClassLoader().getResource("webdriver/mac/chromedriver").getPath());
        WebDriver driver = new ChromeDriver();
        driver.get("https://gmail.com");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String title = driver.getTitle();
        assertEquals(title, "Gmail");
        driver.quit();
    }

    @Test
    public void openGmailTest2() {
        String osName = System.getProperty("os.name");
        if (osName.contains("Windows")) {
        System.setProperty("webdriver.chrome.driver", this.getClass().getClassLoader()
                .getResource("webdriver/windows/chromedriver.exe").getPath());
        } else if (osName.contains("Mac")) {
            System.setProperty("webdriver.chrome.driver", this.getClass().getClassLoader()
                    .getResource("webdriver/mac/chromedriver").getPath());
        } else {
            System.setProperty("webdriver.chrome.driver", this.getClass().getClassLoader()
                    .getResource("webdriver/linux/chromedriver").getPath());
        }

        WebDriver driver = new ChromeDriver();
        driver.get("https://gmail.com");
//        driver.navigate().to("");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String title = driver.getTitle();
        assertEquals(title, "Gmail");
        driver.quit();
    }

    @Test
    public void openGmailTest3() {
        WebDriverManager.chromedriver().setup();
        WebDriver driver = new ChromeDriver();
        driver.get("https://gmail.com");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        String title = driver.getTitle();
        assertEquals(title, "Gmail");
        driver.quit();
    }
}
