package ru.levelup.qa.at.selenium.scripts;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class LocatorTest {

    private WebDriver driver;
    private WebDriverWait wait;

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://gmail.com");
        wait = new WebDriverWait(driver, 15);
    }

    @Test
    public void seleniumDomLocatorTest() {
        WebElement userNameTextFieldById = driver.findElement(By.id("identifierId"));
        userNameTextFieldById.sendKeys("testEmail");
        sleep(3000);

        WebElement userNameTextFieldByName = driver.findElement(By.name("identifier"));
        userNameTextFieldByName.clear();
        userNameTextFieldByName.sendKeys("my new email");
        sleep(3000);

        List<WebElement> listByClassName = driver.findElements(By.className("PrDSKc"));
        for (WebElement webElement : listByClassName) {
            System.out.println(webElement.getText());
        }
        System.out.println();
        sleep(3000);

        WebElement linkByLinkText = driver.findElement(By.linkText("Подробнее…"));
        System.out.println(linkByLinkText.getText());
        System.out.println();

        WebElement linkByPartialLinkText = driver.findElement(By.partialLinkText("одроб"));
        System.out.println(linkByPartialLinkText.getText());
        System.out.println();
    }

    @Test
    public void seleniumCssLocatorTest() {
        WebElement userNameTextFieldById = driver.findElement(By.cssSelector("#identifierId"));
        userNameTextFieldById.sendKeys("testEmail");
        sleep(3000);

        WebElement userNameTextFieldByName = driver.findElement(By.cssSelector("[name='identifier']"));
        userNameTextFieldByName.clear();
        userNameTextFieldByName.sendKeys("my new email");
        sleep(3000);

        List<WebElement> listByClassName = driver.findElements(By.cssSelector(".PrDSKc"));
        for (WebElement webElement : listByClassName) {
            System.out.println(webElement.getText());
        }
        System.out.println();
        sleep(3000);

        WebElement linkByLinkText = driver.findElement(By.cssSelector("div a[jsname='JFyozc']"));
        System.out.println(linkByLinkText.getText());
        System.out.println();

        WebElement linkByPartialLinkText = driver.findElement(By.cssSelector("div[jsname='FIbd0b'] span span.snByac"));
        System.out.println(linkByPartialLinkText.getText());
        System.out.println();
    }

    @Test
    public void seleniumXPathLocatorTest() {
        WebElement userNameTextFieldById = driver.findElement(By.xpath("//*[@id='identifierId']"));
        userNameTextFieldById.sendKeys("testEmail");
        sleep(3000);

        WebElement userNameTextFieldByName = driver.findElement(By.xpath("//input[@name='identifier']"));
        userNameTextFieldByName.clear();
        userNameTextFieldByName.sendKeys("my new email");
        sleep(3000);

        List<WebElement> listByClassName = driver.findElements(By.xpath("//*[@class='PrDSKc']"));
        for (WebElement webElement : listByClassName) {
            System.out.println(webElement.getText());
        }
        System.out.println();
        sleep(3000);

        WebElement linkByLinkText = driver.findElement(By.xpath("//a[text()='Подробнее…']"));
        System.out.println(linkByLinkText.getText());
        System.out.println();

        WebElement linkByPartialLinkText = driver.findElement(By.xpath("//a[.='Подробнее…']"));
        System.out.println(linkByPartialLinkText.getText());
        System.out.println();
    }

    @Test
    public void userBugRedLoginTest() {
        driver.get("http://users.bugred.ru/");

        long startTime = 0L;
        long endTime = 0L;

        startTime = System.currentTimeMillis();
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a//span[text()='Войти']")));
        loginButton.click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//a//span[text()='Войти']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("login"))).sendKeys("test.user@email.com");
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "login", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        WebElement form = wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//form[contains(@action, 'login')]")));
        assertTrue(form.isDisplayed());

        // 1. direct
        form.findElement(By.xpath("input[@name='password']")).sendKeys("test");

        // 2. Wait
        wait.until(ExpectedConditions.visibilityOfNestedElementsLocatedBy(form, By.name("password"))).get(0)
                .sendKeys("test");

        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//form[contains(@action, 'login')]//input[@name='password']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Авторизоваться']"))).click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//input[@value='Авторизоваться']", (endTime - startTime)));

        sleep(3000);

        startTime = System.currentTimeMillis();
        WebElement userButton = null;
        try {
            userButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".dropdown-toggle1")));
        } finally {
            endTime = System.currentTimeMillis();
            System.out.println(String.format("Time for locating element '%s' is %d", ".dropdown-toggle1", (endTime - startTime)));
        }
        assertTrue(userButton.isDisplayed(), "Кнопка пользователя не отображается");
        assertEquals(userButton.getText(), "test user");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
