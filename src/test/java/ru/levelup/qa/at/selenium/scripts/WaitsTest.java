package ru.levelup.qa.at.selenium.scripts;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.checkerframework.checker.nullness.compatqual.NullableDecl;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class WaitsTest {

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        driver.get("https://gmail.com");
        wait = new WebDriverWait(driver, 15);
    }

    @Test
    public void explicitlyWaitTest() {
        WebElement userNameTextField = wait.until(
                ExpectedConditions.visibilityOf(driver.findElement(By.id("identifierId"))));
        userNameTextField.sendKeys("testTester98210");
        WebElement nextButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Далее']")));
        nextButton.click();

        WebElement passwordTextField = wait.until(
                ExpectedConditions.visibilityOf(driver.findElement(By.name("password"))));
        passwordTextField.sendKeys("QWEqwe123");
        nextButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//*[text()='Далее']")));
        nextButton.click();

        sleep(3000);

        WebElement userIcon = driver.findElement(By.xpath("//a[contains(@aria-lable, 'testTester98210')]"));
        wait.until(ExpectedConditions.visibilityOf(userIcon));

        assertTrue(userIcon.isDisplayed());
    }

    @Test
    public void userBugRedLoginTest() {
        driver.get("http://users.bugred.ru/");

        long startTime = 0L;
        long endTime = 0L;

        startTime = System.currentTimeMillis();
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a//span[text()='Войти']")));
        loginButton.click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//a//span[text()='Войти']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("login"))).sendKeys("test.user@email.com");
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "login", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//form[contains(@action, 'login')]//input[@name='password']")))
                .sendKeys("test");
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//form[contains(@action, 'login')]//input[@name='password']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Авторизоваться']"))).click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//input[@value='Авторизоваться']", (endTime - startTime)));

        sleep(3000);

        startTime = System.currentTimeMillis();
        WebElement userButton = null;
        try {
            userButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".dropdown-toggle1")));
        } finally {
            endTime = System.currentTimeMillis();
            System.out.println(String.format("Time for locating element '%s' is %d", ".dropdown-toggle1", (endTime - startTime)));
        }
        assertTrue(userButton.isDisplayed(), "Кнопка пользователя не отображается");
        assertEquals(userButton.getText(), "test user");
    }

    @Test
    public void userBugRedLoginCustomWaitTest() {
        driver.get("http://users.bugred.ru/");

        long startTime = 0L;
        long endTime = 0L;

        startTime = System.currentTimeMillis();
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a//span[text()='Войти']")));
        loginButton.click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//a//span[text()='Войти']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("login"))).sendKeys("test.user@email.com");
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "login", (endTime - startTime)));

        startTime = System.currentTimeMillis();
//        wait.until(ExpectedConditions
//                .visibilityOfElementLocated(By.xpath("//form[contains(@action, 'login')]//input[@name='password']")))
//                .clear();
//        wait.until(ExpectedConditions
//                .visibilityOfElementLocated(By.xpath("//form[contains(@action, 'login')]//input[@name='password']")))
//                .sendKeys("test");

        WebElement passwordTextField = wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//form[contains(@action, 'login')]//input[@name='password']")));
        Actions actions = new Actions(driver);
        actions
                .click(passwordTextField)
                .sendKeys("test")
                .build()
                .perform();

        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//form[contains(@action, 'login')]//input[@name='password']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Авторизоваться']"))).click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//input[@value='Авторизоваться']", (endTime - startTime)));

        sleep(3000);

        startTime = System.currentTimeMillis();
        WebElement userButton = null;
        try {
            userButton = wait.until(new ExpectedCondition<WebElement>() {
                @NullableDecl
                @Override
                public WebElement apply(@NullableDecl WebDriver driver) {
                    WebElement element = driver.findElement(By.cssSelector(".dropdown-toggle1"));
                    return element.isDisplayed() ? element : null;
                }
            });
        } finally {
            endTime = System.currentTimeMillis();
            System.out.println(String.format("Time for locating element '%s' is %d", ".dropdown-toggle1", (endTime - startTime)));
        }
        assertTrue(userButton.isDisplayed(), "Кнопка пользователя не отображается");
        assertEquals(userButton.getText(), "test user");
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
