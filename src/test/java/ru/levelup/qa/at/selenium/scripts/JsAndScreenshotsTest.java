package ru.levelup.qa.at.selenium.scripts;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.StaleElementReferenceException;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import java.io.File;
import java.io.IOException;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class JsAndScreenshotsTest {

    private WebDriver driver;

    private WebDriverWait wait;

    @BeforeMethod
    public void setUp() {
        WebDriverManager.chromedriver().setup();
        driver = new ChromeDriver();
        wait = new WebDriverWait(driver, 15);
    }

    @Test
    public void userBugRedLogoutTest() {
        driver.get("http://users.bugred.ru/");

        long startTime = 0L;
        long endTime = 0L;

        startTime = System.currentTimeMillis();
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a//span[text()='Войти']")));
        loginButton.click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//a//span[text()='Войти']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("login"))).sendKeys("test.user@email.com");
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "login", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//form[contains(@action, 'login')]//input[@name='password']")))
                .sendKeys("test");
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//form[contains(@action, 'login')]//input[@name='password']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Авторизоваться']"))).click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//input[@value='Авторизоваться']", (endTime - startTime)));

        sleep(3000);

        startTime = System.currentTimeMillis();
        WebElement userButton = null;
        try {
            userButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".dropdown-toggle")));
        } finally {
            endTime = System.currentTimeMillis();
            System.out.println(String.format("Time for locating element '%s' is %d", ".dropdown-toggle1", (endTime - startTime)));
        }
        assertTrue(userButton.isDisplayed(), "Кнопка пользователя не отображается");
        assertEquals(userButton.getText(), "test user");

        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", userButton);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Выход"))).click();

        boolean loginButtonDisplayed;

        try {
            loginButtonDisplayed = loginButton.isDisplayed();
        } catch (StaleElementReferenceException e) {
            loginButtonDisplayed = wait.until(ExpectedConditions
                    .visibilityOfElementLocated(By.xpath("//a//span[text()='Войти']")))
                    .isDisplayed();
        }

        assertTrue(loginButtonDisplayed, "Кнопка Войти не отображается");
    }

    @Test
    public void userBugRedLogoutScreenShotTest() throws IOException {
        driver.get("http://users.bugred.ru/");

        long startTime = 0L;
        long endTime = 0L;

        startTime = System.currentTimeMillis();
        WebElement loginButton = wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a//span[text()='Войти']")));
        loginButton.click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//a//span[text()='Войти']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.name("login"))).sendKeys("test.user@email.com");
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "login", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions
                .visibilityOfElementLocated(By.xpath("//form[contains(@action, 'login')]//input[@name='password']")))
                .sendKeys("test");
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//form[contains(@action, 'login')]//input[@name='password']", (endTime - startTime)));

        startTime = System.currentTimeMillis();
        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//input[@value='Авторизоваться']"))).click();
        endTime = System.currentTimeMillis();
        System.out.println(String.format("Time for locating element '%s' is %d", "//input[@value='Авторизоваться']", (endTime - startTime)));

        sleep(3000);

        startTime = System.currentTimeMillis();
        WebElement userButton = null;
        try {
            userButton = wait.until(ExpectedConditions.visibilityOfElementLocated(By.cssSelector(".dropdown-toggle")));
        } finally {
            endTime = System.currentTimeMillis();
            System.out.println(String.format("Time for locating element '%s' is %d", ".dropdown-toggle1", (endTime - startTime)));
        }
        assertTrue(userButton.isDisplayed(), "Кнопка пользователя не отображается");
        assertEquals(userButton.getText(), "test user");

        ((JavascriptExecutor) driver).executeScript("arguments[0].click()", userButton);
        wait.until(ExpectedConditions.elementToBeClickable(By.linkText("Выход"))).click();

        try {
            assertTrue(loginButton.isDisplayed(), "Кнопка Войти не отображается");
        } catch (StaleElementReferenceException e) {
            TakesScreenshot ts = (TakesScreenshot) driver;
            File screenshotAsFile = ts.getScreenshotAs(OutputType.FILE);
            FileUtils.copyFile(screenshotAsFile, new File("target/MyScreenshot.png"));
        }
    }

    @AfterMethod
    public void tearDown() {
        driver.quit();
    }

    void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
