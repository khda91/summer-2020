package ru.levelup.qa.at.selenium.po.fluent;

import org.testng.annotations.Test;
import ru.levelup.qa.at.selenium.po.AbstractBaseTest;

import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class UserBugRedPageObjectTest extends AbstractBaseTest {

    @Test
    public void userBugRedLoginTest() {
        String displayedUsername = new HomePage(driver)
                .open()
                .clickEnterButton()
                .successfulLogin("test.user@email.com", "test")
                .getTextUserButton();

        assertEquals(displayedUsername, "test user");
    }

    @Test
    public void userBugRedLoginReadabilityTest() {
        LoginRegistrationPage loginRegistrationPage = new HomePage(driver)
                .open()
                .clickEnterButton();

        String displayedUsername = loginRegistrationPage.successfulLogin("test.user@email.com", "test")
                .getTextUserButton();

        assertEquals(displayedUsername, "test user");
    }

    @Test
    public void userBugRedLoginNegativeTest() {
        boolean authorizationButtonDisplayed = new HomePage(driver)
                .open()
                .clickEnterButton()
                .wrongLogin("test.user@email.com", "hygfhdsbvabdskjvbj")
                .isAuthorizationButtonDisplayed();

        assertTrue(authorizationButtonDisplayed);
    }

    @Test
    public void userBugRedLogoutTest() {
        boolean enterButtonDisplayed = new HomePage(driver)
                .open()
                .clickEnterButton()
                .successfulLogin("test.user@email.com", "test")
                .clickUserButton()
                .clickExitButton()
                .isEnterButtonDisplayed();

        assertTrue(enterButtonDisplayed);
    }

}
