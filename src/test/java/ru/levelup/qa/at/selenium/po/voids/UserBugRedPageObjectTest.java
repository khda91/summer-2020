package ru.levelup.qa.at.selenium.po.voids;

import org.testng.annotations.Test;
import ru.levelup.qa.at.selenium.po.AbstractBaseTest;


import static org.testng.Assert.assertEquals;
import static org.testng.Assert.assertTrue;

public class UserBugRedPageObjectTest extends AbstractBaseTest {

    @Test
    public void userBugRedLoginTest() {
        HomePage homePage = new HomePage(driver);

        homePage.open();
        homePage.clickEnterButton();

        LoginRegistrationPage loginRegistrationPage = new LoginRegistrationPage(driver);
        loginRegistrationPage.login("test.user@email.com", "test");

        assertEquals(homePage.getTextUserButton(), "test user");
    }

    @Test
    public void userBugRedLogoutTest() {
        HomePage homePage = new HomePage(driver);

        homePage.open();
        homePage.clickEnterButton();

        LoginRegistrationPage loginRegistrationPage = new LoginRegistrationPage(driver);
        loginRegistrationPage.login("test.user@email.com", "test");

        homePage.clickUserButton();
        homePage.clickExitButton();

        assertTrue(homePage.isEnterButtonDisplayed());
    }

}
