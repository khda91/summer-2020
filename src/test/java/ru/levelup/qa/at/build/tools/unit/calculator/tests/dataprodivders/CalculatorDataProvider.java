package ru.levelup.qa.at.build.tools.unit.calculator.tests.dataprodivders;

import org.testng.annotations.DataProvider;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

public class CalculatorDataProvider {

    @DataProvider
    public Object[][] divideCalculatorDataProvider() {
        return new Object[][]{
                {4.0, -2.0, -2.0}
        };
    }

    @DataProvider
    public Object[][] divideCalculatorDataProviderFromFile() {
        Object[][] testData = null;
        List<String> fileData = new ArrayList<>();
        String line;
        BufferedReader br = new BufferedReader(new InputStreamReader(this.getClass().getClassLoader().getResourceAsStream("test-data.csv")));

        try {
            while ((line = br.readLine()) != null) {
                fileData.add(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }

        testData = new Object[fileData.size()][3];

        for (int i = 0; i < testData.length; i++) {
            String[] data = fileData.get(i).split(",");
            Object[] objs = new Object[3];
            for (int j = 0; j < objs.length; j++) {
                objs[j] = Double.parseDouble(data[j]);
            }
            testData[i] = objs;
        }

        return testData;
    }

}
