package ru.levelup.qa.at.build.tools.integration.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class AddCalculatorIT extends BaseCalculatorIT {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest AddCalculatorIT");
    }

    @Test
    public void addCalculatorTest() {
        System.out.println("addCalculatorTest IT");
        double result = calculator.add(2.0, 2.0);
        Assert.assertEquals(result, 4.0D, 0.0000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest AddCalculatorIT");
    }

}
