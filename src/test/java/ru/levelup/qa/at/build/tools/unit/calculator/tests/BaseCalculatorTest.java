package ru.levelup.qa.at.build.tools.unit.calculator.tests;

import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeSuite;
import ru.levelup.qa.at.build.tools.units.calculator.Calculator;

public abstract class BaseCalculatorTest {

    protected Calculator calculator;

    @BeforeSuite
    public void setUpBeforeSuite() {
        System.out.println("setUpBeforeSuite");
    }

    @BeforeClass
    public void setUpBeforeClass() {
        System.out.println("setUpBeforeClass");
    }

    @BeforeMethod
    public void setUpBeforeMethod() {
        System.out.println("setUpBeforeMethod");
        calculator = new Calculator();
    }

    @AfterMethod
    public void tearDownAfterMethod() {
        System.out.println("tearDownAfterMethod");
        calculator = null;
    }

    @AfterClass
    public void tearDownAfterClass() {
        System.out.println("tearDownAfterClass");
    }

    @AfterSuite
    public void tearDownAfterSuite() {
        System.out.println("tearDownAfterSuite");
    }
}
