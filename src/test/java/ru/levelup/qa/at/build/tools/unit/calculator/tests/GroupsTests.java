package ru.levelup.qa.at.build.tools.unit.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.BeforeGroups;
import org.testng.annotations.Test;
import ru.levelup.qa.at.build.tools.units.calculator.Calculator;

public class GroupsTests {

    @BeforeGroups(groups = {"calc"})
    public void beforeGroup() {
        System.out.println("before groups");
    }

    @Test(groups = {"add", "calc"})
    public void additionGroupTest() {
        Calculator calculator = new Calculator();
        double add = calculator.add(2, 2);
        Assert.assertEquals(add, 4, 0.000001);
    }

    @Test(groups = {"add", "calc"})
    public void additionGroupTest1() {
        Calculator calculator = new Calculator();
        double add = calculator.add(2, 2);
        Assert.assertEquals(add, 4, 0.000001);
    }

    @Test(groups = {"mult", "calc"})
    public void multiplyGroupTest() {
        Calculator calculator = new Calculator();
        double add = calculator.multiply(2, 2);
        Assert.assertEquals(add, 4, 0.000001);
    }
}
