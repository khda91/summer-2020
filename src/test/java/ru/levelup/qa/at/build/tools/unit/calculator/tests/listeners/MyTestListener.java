package ru.levelup.qa.at.build.tools.unit.calculator.tests.listeners;

import org.testng.ITestListener;
import org.testng.ITestResult;

public class MyTestListener implements ITestListener {

    @Override
    public void onTestStart(ITestResult result) {
        System.out.println("Start");
        System.out.println("Test: " + result.getTestName());
    }

    @Override
    public void onTestSuccess(ITestResult result) {
        System.out.println("Test: " + result.getTestName() + " passed");
    }

    @Override
    public void onTestFailure(ITestResult result) {
        // make screenshot
        System.out.println("Test: " + result.getTestName() + " failed");
    }
}
