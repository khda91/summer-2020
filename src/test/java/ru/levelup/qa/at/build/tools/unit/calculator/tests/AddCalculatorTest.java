package ru.levelup.qa.at.build.tools.unit.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class AddCalculatorTest extends BaseCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest AddCalculatorTest");
    }

    @Test
    @Parameters({"a", "b", "expected"})
    public void addCalculatorTest(double a, double b, double expected) {
        System.out.println("addCalculatorTest");
        double result = calculator.add(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest AddCalculatorTest");
    }

}
