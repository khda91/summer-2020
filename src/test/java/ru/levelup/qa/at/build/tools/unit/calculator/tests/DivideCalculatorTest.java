package ru.levelup.qa.at.build.tools.unit.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import ru.levelup.qa.at.build.tools.unit.calculator.tests.dataprodivders.CalculatorDataProvider;

public class DivideCalculatorTest extends BaseCalculatorTest {

    @Test(testName = "Divide",
            dataProviderClass = CalculatorDataProvider.class, dataProvider = "divideCalculatorDataProvider")
    public void divideCalculatorTest(double a, double b, double expected) {
        System.out.println("divideCalculatorTest");
        double result = calculator.divide(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @Test(testName = "Divide from file",
            dataProviderClass = CalculatorDataProvider.class, dataProvider = "divideCalculatorDataProviderFromFile")
    public void divideCalculatorFromFileTest(double a, double b, double expected) {
        System.out.println("divideCalculatorFromFileTest");
        double result = calculator.divide(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @DataProvider
    public Object[][] divideTestDataProvider() {
        return new Object[][] {
                {2L, 2L, 1L}
        };
    }

    @Test(dataProvider = "divideTestDataProvider")
    public void dividePositiveTest(long a, long b, long expected) {
        System.out.println("dividePositiveTest");
        long result = calculator.divide(a, b);
        Assert.assertEquals(result, expected);
    }

    @Test(dataProvider = "divideTestDataProvider",
            expectedExceptions = ArithmeticException.class)
    public void divideNegativeTest(long a, long b, long expected) {
        System.out.println("divideNegativeTest");
        long result = calculator.divide(a, 0);
    }
}
