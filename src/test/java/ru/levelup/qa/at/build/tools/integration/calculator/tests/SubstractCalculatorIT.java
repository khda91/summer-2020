package ru.levelup.qa.at.build.tools.integration.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SubstractCalculatorIT extends BaseCalculatorIT {

    @Test
    public void substractCalculatorTest() {
        System.out.println("substractCalculatorTest IT");
        double result = calculator.substract(2.0, 6.0);
        Assert.assertEquals(result, -5.0D, 0.0000001);
    }
}
