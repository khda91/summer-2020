package ru.levelup.qa.at.build.tools.unit.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.Test;
import ru.levelup.qa.at.build.tools.units.calculator.Calculator;

public class DependenciesTests {

    @Test
    public void additionDependentTest() {
        Calculator calculator = new Calculator();
        double add = calculator.add(2, 2);
        Assert.assertEquals(add, 4, 0.000001);
    }

    @Test(dependsOnMethods = "multiplyDependentTest")
    public void additionDependentTest1() {
        Calculator calculator = new Calculator();
        double add = calculator.add(2, 2);
        Assert.assertEquals(add, 4, 0.000001);
    }

    @Test
    public void additionDependentTest2() {
        Calculator calculator = new Calculator();
        double add = calculator.add(2, 2);
        Assert.assertEquals(add, 4, 0.000001);
    }

    @Test(dependsOnMethods = {"additionDependentTest", "additionDependentTest2"})
    public void multiplyDependentTest() {
        Calculator calculator = new Calculator();
        double add = calculator.multiply(2, 2);
        Assert.assertEquals(add, -4, 0.000001);
    }
}
