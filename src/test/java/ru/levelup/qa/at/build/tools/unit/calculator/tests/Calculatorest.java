package ru.levelup.qa.at.build.tools.unit.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.AfterClass;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.levelup.qa.at.build.tools.units.calculator.Calculator;

public class Calculatorest {

    private Calculator calculator;

    @BeforeClass
    public void setUpBeforeClass() {
        System.out.println("setUpBeforeClass");
    }

    @BeforeMethod
    public void setUpBeforeMethod() {
        System.out.println("setUpBeforeMethod");
        calculator = new Calculator();
    }

    @Test
    public void addCalculatorTest() {
        System.out.println("addCalculatorTest");
        double result = calculator.add(2.0, 2.0);
        Assert.assertEquals(result, 4.0D, 0.0000001);
    }

    @Test
    public void multiplyCalculatorTest() {
        System.out.println("multiplyCalculatorTest");
        double result = calculator.multiply(2.0, 6.0);
        Assert.assertEquals(result, 12.0D, 0.0000001);
    }

    @AfterMethod
    public void tearDownAfterMethod() {
        System.out.println("tearDownAfterMethod");
        calculator = null;
    }

    @AfterClass
    public void tearDownAfterClass() {
        System.out.println("tearDownAfterClass");
    }
}
