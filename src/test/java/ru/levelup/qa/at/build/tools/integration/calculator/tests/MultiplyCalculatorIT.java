package ru.levelup.qa.at.build.tools.integration.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

public class MultiplyCalculatorIT extends BaseCalculatorIT {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest MultiplyCalculatorIT");
    }

    @Test
    public void multiplyCalculatorTest() {
        System.out.println("multiplyCalculatorTest IT");
        double result = calculator.multiply(2.0, 6.0);
        Assert.assertEquals(result, 12.0D, 0.0000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest MultiplyCalculatorIT");
    }

}
