package ru.levelup.qa.at.build.tools.unit.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.Test;

public class SubstractCalculatorTest extends BaseCalculatorTest {

    @Test
    public void substractCalculatorTest() {
        System.out.println("substractCalculatorTest");
        double result = calculator.substract(2.0, 6.0);
        Assert.assertEquals(result, -4.0D, 0.0000001);
    }
}
