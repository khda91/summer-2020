package ru.levelup.qa.at.build.tools.unit.calculator.tests;

import org.testng.Assert;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Listeners;
import org.testng.annotations.Test;
import ru.levelup.qa.at.build.tools.unit.calculator.tests.listeners.MyTestListener;

@Listeners({MyTestListener.class})
public class MultiplyCalculatorTest extends BaseCalculatorTest {

    @BeforeTest
    public void setUpBeforeTest() {
        System.out.println("setUpBeforeTest MultiplyCalculatorTest");
    }

    @DataProvider
    public Object[][] dataProvider() {
        return new Object[][] {
                {2.0, 6.0, 12.0},
                {6.1, 6.0, 36.1},
                {11, 11, 121}
        };
    }

    @Test(dataProvider = "dataProvider")
    public void multiplyCalculatorTest(double a, double b, double expected) {
        System.out.println("multiplyCalculatorTest");
        double result = calculator.multiply(a, b);
        Assert.assertEquals(result, expected, 0.0000001);
    }

    @DataProvider(name = "Data Provider for Negative multiply calculator operation test")
    public Object[][] dataProviderNegative() {
        return new Object[][] {
                {2.0, 6.0, 11.0},
                {6.1, 6.0, 36.1},
                {11, 11, 122}
        };
    }

    @Test(dataProvider = "Data Provider for Negative multiply calculator operation test")
    public void multiplyCalculatorNegativeTest(double a, double b, double expected) {
        System.out.println("multiplyCalculatorNegativeTest");
        double result = calculator.multiply(a, b);
        Assert.assertNotEquals(result, expected, 0.0000001);
    }

    @AfterTest
    public void tearDownAfterTest() {
        System.out.println("tearDownAfterTest MultiplyCalculatorTest");
    }

}
