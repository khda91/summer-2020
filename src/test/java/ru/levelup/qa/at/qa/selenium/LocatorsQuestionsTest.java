package ru.levelup.qa.at.qa.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import ru.levelup.qa.at.selenium.po.AbstractBaseTest;

import java.util.List;

public class LocatorsQuestionsTest extends AbstractBaseTest {

    @Test
    public void itemsTest() {
        driver.navigate().to("https://mail.ru");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:login"))).sendKeys("testaccount_2020");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:password"))).sendKeys("HOqnrwa3znQtwQ3vJHbA");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();

        wait.until(ExpectedConditions.titleContains("Входящие"));

        sleep(3000);

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//div[@class='nav__folder-name']/div[text()='Test']"))).click();

        sleep(3000);

        WebElement inboxFolder = wait.until(ExpectedConditions.presenceOfElementLocated(By.xpath("//*[@href='/inbox/']")));
        inboxFolder.click();

        sleep(3000);
    }
}
