package ru.levelup.qa.at.qa.selenide;

import com.codeborne.selenide.Selenide;
import org.openqa.selenium.Keys;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.matchesText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.actions;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.title;

public class SelenideHotKeys {

    @Test
    public void selenideTitleTest() {
        open("https://mail.ru");

        $("#mailbox\\:login").clear();
        $("#mailbox\\:login").sendKeys("testaccount_2020");
        $("[value='Ввести пароль']").click();
        $("#mailbox\\:password").sendKeys("HOqnrwa3znQtwQ3vJHbA");
        $("[value='Ввести пароль']").click();

        String pageTitle = title();
        $x("//title").waitWhile(matchesText("\\*Входящие\\*"), 15000);

        System.out.println("Current page title is: " + pageTitle);

        actions().keyDown(Keys.COMMAND).keyDown("N").build().perform();

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
