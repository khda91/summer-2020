package ru.levelup.qa.at.qa.selenium;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.levelup.qa.at.selenium.po.AbstractBaseTest;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

import static org.openqa.selenium.By.cssSelector;
import static org.openqa.selenium.By.partialLinkText;
import static org.openqa.selenium.By.xpath;
import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfElementLocated;
import static org.openqa.selenium.support.ui.ExpectedConditions.presenceOfNestedElementLocatedBy;

public class YandexMarketSampleTest extends AbstractBaseTest {

    @Test
    public void compareProductsTest() {
        driver.get("https://market.yandex.ru/");

        wait.until(elementToBeClickable(xpath("//div[@data-zone-name='category-link']//span[text()='Компьютеры']"))).click();
        wait.until(elementToBeClickable(xpath("//div[@data-zone-name='link']//a[text() = 'Ноутбуки']"))).click();

        List<WebElement> productsList = driver.findElements(cssSelector("article[data-zone-name='snippet-card']"));
        List<String> productsNames = new ArrayList<>();
        Actions actions = new Actions(driver);

        String product1 = productsList.get(0).findElement(cssSelector("[data-zone-name='title']")).getText().trim();
        WebElement addToCompareButton = wait.until(presenceOfNestedElementLocatedBy(productsList.get(0),
                xpath("div[2]/div[contains(@aria-label, 'Добавить')]")));
        actions
                .moveToElement(addToCompareButton)
                .click(addToCompareButton)
                .build()
                .perform();
        productsNames.add(product1);

        String product2 = productsList.get(1).findElement(cssSelector("[data-zone-name='title']")).getText().trim();
        addToCompareButton = wait.until(presenceOfNestedElementLocatedBy(productsList.get(1),
                xpath("div[2]/div[contains(@aria-label, 'Добавить')]")));
        actions
                .moveToElement(addToCompareButton)
                .click(addToCompareButton)
                .build()
                .perform();
        productsNames.add(product2);

        wait.until(elementToBeClickable(partialLinkText("Сравнить"))).click();

        List<WebElement> actualWebElementList = driver.findElements(xpath("//img/..//a[contains(@class, 'cia-cs')]"));
        List<String> actualProductNames = actualWebElementList
                .stream()
                .map(WebElement::getText)
                .collect(Collectors.toList());

        Assert.assertEqualsNoOrder(actualProductNames.toArray(), productsNames.toArray());
    }
}
