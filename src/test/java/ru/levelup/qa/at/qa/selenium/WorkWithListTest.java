package ru.levelup.qa.at.qa.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import ru.levelup.qa.at.selenium.po.AbstractBaseTest;

import java.util.List;

public class WorkWithListTest extends AbstractBaseTest {

    @Test
    public void itemsTest() {
        driver.navigate().to("https://mail.ru");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:login"))).sendKeys("testaccount_2020");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:password"))).sendKeys("HOqnrwa3znQtwQ3vJHbA");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();

        wait.until(ExpectedConditions.titleContains("Входящие"));

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@title, 'Test')]"))).click();

        wait.until(ExpectedConditions.titleContains("Test"));

        List<WebElement> unreadedList = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("a.js-letter-list-item"), 10));
        System.out.println("Unreader emails in Test folder is " + unreadedList.size());

        List<WebElement> subjectLocal = unreadedList.get(0).findElements(By.xpath("div[@class='llc__container']//*[contains(@class, 'llc__subject')]"));
        List<WebElement> subjectWhole = unreadedList.get(0).findElements(By.xpath("//*[contains(@class, 'llc__subject')]"));

        System.out.println("subject local " + subjectLocal.size());
        System.out.println("subject whole " + subjectWhole.size());

        for (WebElement email : unreadedList) {

        }

    }
}
