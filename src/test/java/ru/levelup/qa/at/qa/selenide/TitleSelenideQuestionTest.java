package ru.levelup.qa.at.qa.selenide;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.matchesText;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.Selenide.title;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class TitleSelenideQuestionTest {

    @Test
    public void selenideTitleTest() {
        open("https://mail.ru");

        $("#mailbox\\:login").clear();
        $("#mailbox\\:login").sendKeys("testaccount_2020");
        $("[value='Ввести пароль']").click();
        $("#mailbox\\:password").sendKeys("HOqnrwa3znQtwQ3vJHbA");
        $("[value='Ввести пароль']").click();

        String pageTitle = title();
        $x("//title").waitWhile(matchesText("\\*Входящие\\*"), 15000);

        System.out.println("Current page title is: " + pageTitle);
    }

    @Test
    public void selenideTitleWithSeleniumWaitTest() {
        open("https://mail.ru");

        $("#mailbox\\:login").clear();
        $("#mailbox\\:login").sendKeys("testaccount_2020");
        $("[value='Ввести пароль']").click();
        $("#mailbox\\:password").sendKeys("HOqnrwa3znQtwQ3vJHbA");
        $("[value='Ввести пароль']").click();

        String pageTitle = title();

        WebDriver driver = getWebDriver();
        new WebDriverWait(driver, 15).until(ExpectedConditions.titleContains("Входящие"));

        System.out.println("Current page title is: " + pageTitle);
    }
}
