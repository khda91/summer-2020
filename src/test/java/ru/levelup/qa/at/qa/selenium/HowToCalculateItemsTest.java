package ru.levelup.qa.at.qa.selenium;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.testng.annotations.Test;
import ru.levelup.qa.at.selenium.po.AbstractBaseTest;

import java.util.List;

public class HowToCalculateItemsTest extends AbstractBaseTest {

    @Test
    public void itemsTest() {
        driver.navigate().to("https://mail.ru");

        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:login"))).sendKeys("testaccount_2020");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:password"))).sendKeys("HOqnrwa3znQtwQ3vJHbA");
        wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("mailbox:submit"))).click();

        wait.until(ExpectedConditions.titleContains("Входящие"));

        wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//a[contains(@title, 'Test')]"))).click();

        wait.until(ExpectedConditions.titleContains("Test"));

//        wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("div.llc__item_unread"), 10));

//        List<WebElement> unreadedList = driver.findElements(By.cssSelector("div.llc__item_unread"));
        List<WebElement> unreadedList = wait.until(ExpectedConditions.numberOfElementsToBeMoreThan(By.cssSelector("div.llc__item_unread"), 10));
        System.out.println("Unreader emails in Test folder is " + unreadedList.size());

        List<WebElement> wholeList = driver.findElements(By.cssSelector("a.js-letter-list-item"));
        System.out.println("Emails in Test folder is " + wholeList.size());

        List<WebElement> unreadedList2 = driver.findElements(By.xpath("//div[@class='llc__item llc__item_correspondent llc__item_unread']"));
        System.out.println("Unreader2 emails in Test folder is " + unreadedList2.size());

        ((JavascriptExecutor) driver).executeScript("window.scrollTo(0, 300);");

        sleep(5000);

        wholeList = driver.findElements(By.cssSelector("a.js-letter-list-item"));
        System.out.println("Emails in Test folder is " + wholeList.size());
    }
}
