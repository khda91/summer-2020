package ru.levelup.qa.at.assertions.assertj;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.internal.mapping.GsonMapper;
import io.restassured.path.json.mapper.factory.DefaultGsonObjectMapperFactory;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import ru.levelup.qa.at.api.rqrs.ListUsersResponse;
import ru.levelup.qa.at.api.rqrs.User;

import java.util.Arrays;
import java.util.List;

import static io.restassured.RestAssured.given;
import static org.assertj.core.api.Assertions.assertThat;
import static ru.levelup.qa.at.assertions.Endpoints.BASE_REQ_RES_ENDPOINT;
import static ru.levelup.qa.at.assertions.Endpoints.USERS_ENDPOINT;

public class AssertJStreamAssertionExample {

    RequestSpecification rqSpec;
    ResponseSpecification rsSpec;

    @BeforeMethod
    public void setUp() {
        rqSpec = new RequestSpecBuilder()
                .setBaseUri(BASE_REQ_RES_ENDPOINT)
                .log(LogDetail.ALL)
                .build();

        rsSpec = new ResponseSpecBuilder()
                .expectStatusCode(201)
                .expectContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }

    @Test
    public void assertJStreamApiAssertion() {
        ListUsersResponse usersResponse = given()
                .spec(rqSpec)
                .queryParam("page", 2)
                .when()
                .get(USERS_ENDPOINT)
                .as(ListUsersResponse.class, new GsonMapper(new DefaultGsonObjectMapperFactory()));

        System.out.println(usersResponse);

        List<String> expectedEmails = Arrays.asList("michael.lawson@reqres.in", "lindsay.ferguson@reqres.in",
                "tobias.funke@reqres.in", "byron.fields@reqres.in", "george.edwards@reqres.in", "rachel.howell@reqres.in");

        assertThat(usersResponse.getData())
                .extracting(User::getEmail)
                .hasSize(expectedEmails.size())
                .isEqualTo(expectedEmails);
    }
}
