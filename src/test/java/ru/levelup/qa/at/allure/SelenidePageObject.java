package ru.levelup.qa.at.allure;

import com.codeborne.selenide.Configuration;
import com.codeborne.selenide.logevents.SelenideLogger;
import io.qameta.allure.Attachment;
import io.qameta.allure.Feature;
import io.qameta.allure.Issue;
import io.qameta.allure.Story;
import io.qameta.allure.TmsLink;
import io.qameta.allure.selenide.AllureSelenide;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.title;
import static ru.levelup.qa.at.allure.LoginRegistrationPage.authorizationButton;
import static ru.levelup.qa.at.allure.LoginRegistrationPage.passwordTextField;
import static ru.levelup.qa.at.allure.LoginRegistrationPage.usernameTextField;

@Feature("Selenide Alure")
public class SelenidePageObject {

    @BeforeSuite
    public void beforeSuite() {
        SelenideLogger.addListener("AllureSelenide",
                new AllureSelenide().savePageSource(true).screenshots(true));
        Configuration.headless=true;

        System.out.println();
        System.out.println(System.getenv("KEY_UI_TEST"));
        System.out.println(System.getenv("PROTECTED_VALUE"));
        System.out.println(System.getenv("PROTECTED_MASKED"));
        System.out.println();
    }

    @Test
    @Story("Login Test")
    @Issue("TS-01")
    public void loginTest() {
        LoginRegistrationPage homePage = new HomePage().open().clickEnterButton();

        usernameTextField.sendKeys("test.user@email.ru");
        passwordTextField.sendKeys("test");
        authorizationButton.click();

        homePage.userButton().shouldHave(text("test name"));
    }

    @Test
    @Story("Login Test 1")
    @TmsLink("TS-02")
    public void loginTest1() {
        LoginRegistrationPage homePage = new HomePage().open().clickEnterButton();

        usernameTextField.sendKeys("test.user@email.ru");
        passwordTextField.sendKeys("test");
        authorizationButton.click();

        getPageTitle();

        homePage.userButtonShouldHaveText("test name");
    }

    @Attachment(value = "Page Title", type = "plain/text", fileExtension = "txt")
    public String getPageTitle() {
        return title();
    }
}
