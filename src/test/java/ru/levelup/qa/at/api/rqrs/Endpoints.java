package ru.levelup.qa.at.api.rqrs;

public final class Endpoints {

    public static final String BASE_REQ_RES_ENDPOINT = "https://reqres.in/api";

    public static final String GET_USER_ENDPOINT = "/users/{id}";

    public static final String USERS_ENDPOINT = "/users";

    private Endpoints() {
    }
}
