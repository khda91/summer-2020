package ru.levelup.qa.at.api.rqrs;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.endsWith;
import static org.hamcrest.Matchers.equalTo;

public class ReqResSimpleGetUserTest {

    @Test
    public void getUserRequestTest() {
        given()
                .baseUri("https://reqres.in/api")
        .when()
                .get("/users/2")
        .then()
                .statusCode(200)
                .body("data.id", equalTo(2))
                .body("data.email", equalTo("janet.weaver@reqres.in"))
                .body("ad.company", containsStringIgnoringCase("StatusCode"));
    }

    @DataProvider
    public Object[][] getUserDataProvider() {
        return new Object[][] {
                {2, "janet.weaver@reqres.in", "StatusCode"},
                {1, "george.bluth@reqres.in", "StatusCode"},
                {12, "rachel.howell@reqres.in", "StatusCode"},
                {5, "charles.morris@reqres.in", "StatusCode"},
                {7, "michael.lawson@reqres.in", "StatusCode"},
        };
    }

    @Test(dataProvider = "getUserDataProvider")
    public void getUserRequestWithDataTest(int expectedId, String expectedEmail, String expectedCompanyName) {
        String user = "/users/{id}";
        given()
                .baseUri("https://reqres.in/api")
                .pathParam("id", expectedId)
        .when()
                .get(user)
        .then()
                .statusCode(200)
                .body("data.id", equalTo(expectedId))
                .body("data.email", equalTo(expectedEmail))
                .body("ad.company", containsStringIgnoringCase(expectedCompanyName));
    }
}
