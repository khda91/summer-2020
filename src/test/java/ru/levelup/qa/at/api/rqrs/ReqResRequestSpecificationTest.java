package ru.levelup.qa.at.api.rqrs;

import io.restassured.RestAssured;
import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;

public class ReqResRequestSpecificationTest {

    RequestSpecification rqSpec;
    ResponseSpecification rsSpec;

    @BeforeMethod
    public void setUp() {
        rqSpec = new RequestSpecBuilder()
                .setBaseUri(Endpoints.BASE_REQ_RES_ENDPOINT)
                .log(LogDetail.ALL)
                .build();

        rsSpec = new ResponseSpecBuilder()
                .expectStatusCode(200)
                .expectContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();

        RestAssured.requestSpecification = rqSpec;
        RestAssured.responseSpecification = rsSpec;
    }

    @DataProvider
    public Object[][] getUserDataProvider() {
        return new Object[][]{
                {2, "janet.weaver@reqres.in", "StatusCode"},
                {1, "george.bluth@reqres.in", "StatusCode"},
                {12, "rachel.howell@reqres.in", "StatusCode"},
                {5, "charles.morris@reqres.in", "StatusCode"},
                {7, "michael.lawson@reqres.in", "StatusCode"},
        };
    }

    @Test(dataProvider = "getUserDataProvider")
    public void getUserRequestWithDataTest(int expectedId, String expectedEmail, String expectedCompanyName) {
        given()
                .spec(rqSpec)
                .pathParam("id", expectedId)
                .when()
                .get(Endpoints.GET_USER_ENDPOINT)
                .then()
                .spec(rsSpec)
                .body("data.id", equalTo(expectedId))
                .body("data.email", equalTo(expectedEmail))
                .body("ad.company", containsStringIgnoringCase(expectedCompanyName));
    }

    @Test
    public void getUserRequestTest() {
        given()
                .queryParam("page", 2)
                .when()
                .get(Endpoints.USERS_ENDPOINT)
                .then()
                .body("page", equalTo(2))
                .body("per_page", equalTo(6));
    }

    @Test
    public void postUserRequestTest() {
        given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body("{\n" +
                        "    \"name\": \"morpheus\",\n" +
                        "    \"job\": \"leader\"\n" +
                        "}")
                .when()
                .post(Endpoints.USERS_ENDPOINT)
                .then()
                .contentType(ContentType.JSON)
                .log().all()
                .statusCode(201)
                .body("name", equalTo("morpheus"))
                .body("job", equalTo("leader"))
                .body("id", notNullValue())
                .body("createdAt", notNullValue());
    }

}
