package ru.levelup.qa.at.api.async;

import io.restassured.response.ValidatableResponse;
import org.testng.annotations.Test;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static io.restassured.RestAssured.given;
import static org.hamcrest.Matchers.containsStringIgnoringCase;
import static org.hamcrest.Matchers.equalTo;

public class AsyncExample {

    private static final long TIMEOUT_REQUEST_MILLIS = 60000;

    @Test
    public void getUserRequestTest() {
        ValidatableResponse response = given()
                .baseUri("https://reqres.in/api")
                .when()
                .get("/users/2")
                .then();

        long currentTime = System.currentTimeMillis();
        long endTime = currentTime + TIMEOUT_REQUEST_MILLIS;

        while (currentTime < endTime) {
            if (response.extract().statusCode() == 200) {
                break;
            }
            sleep(1000);
            currentTime = System.currentTimeMillis();
        }

        response.body("data.id", equalTo(2))
                .body("data.email", equalTo("janet.weaver@reqres.in"))
                .body("ad.company", containsStringIgnoringCase("StatusCode"));
    }

    private void sleep(int millis) {
        try {
            TimeUnit.MILLISECONDS.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
