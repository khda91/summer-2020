package ru.levelup.qa.at.api.rqrs;

import io.restassured.builder.RequestSpecBuilder;
import io.restassured.builder.ResponseSpecBuilder;
import io.restassured.filter.log.LogDetail;
import io.restassured.http.ContentType;
import io.restassured.specification.RequestSpecification;
import io.restassured.specification.ResponseSpecification;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static io.restassured.RestAssured.given;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.notNullValue;
import static org.hamcrest.Matchers.samePropertyValuesAs;

public class ReqResSerializationDeserializationTest {

    RequestSpecification rqSpec;
    ResponseSpecification rsSpec;

    @BeforeMethod
    public void setUp() {
        rqSpec = new RequestSpecBuilder()
                .setBaseUri(Endpoints.BASE_REQ_RES_ENDPOINT)
                .log(LogDetail.ALL)
                .build();

        rsSpec = new ResponseSpecBuilder()
                .expectStatusCode(201)
                .expectContentType(ContentType.JSON)
                .log(LogDetail.ALL)
                .build();
    }

    @Test
    public void postUserRequestObjectToJsonTest() {
        given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(new UserRequest("morpheus", "leader"))
                .when()
                .post(Endpoints.USERS_ENDPOINT)
                .then()
                .spec(rsSpec)
                .body("name", equalTo("morpheus"))
                .body("job", equalTo("leader"))
                .body("id", notNullValue())
                .body("createdAt", notNullValue());
    }

    @Test
    public void postUserRequestJsonToObjectTest() {
        UserRequest request = new UserRequest("morpheus", "leader");
        UserResponse responseBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(Endpoints.USERS_ENDPOINT)
                .as(UserResponse.class);

        System.out.println(responseBody);

        assertThat(responseBody.getJob(), equalTo(request.getJob()));
        assertThat(responseBody.getName(), equalTo(request.getName()));

//                .then()
//                .spec(rsSpec)
//                .body("name", equalTo("morpheus"))
//                .body("job", equalTo("leader"))
//                .body("id", notNullValue())
//                .body("createdAt", notNullValue());
    }

    @Test
    public void getUserRequestTest() {
        ListUsersResponse usersResponse = given()
                .spec(rqSpec)
                .queryParam("page", 2)
                .when()
                .get(Endpoints.USERS_ENDPOINT)
                .as(ListUsersResponse.class);


        System.out.println(usersResponse);
//                .then()
//                .body("page", equalTo(2))
//                .body("per_page", equalTo(6));
    }

    @DataProvider
    public Object[][] entityDataProvider() {
        return new Object[][] {
                {new UserRequest("Sveta", "QA"), new UserResponse("", "Sveta", "QA", "")},
                {new UserRequest("Vasya", "Dev"), new UserResponse("", "Vasya", "Dev", "")},
                {new UserRequest("Lesha", "DevOps"), new UserResponse("", "Lesha", "DevOps", "")},
                {new UserRequest("Kolya", "BA"), new UserResponse("", "Kolya", "BA", "")},
                {new UserRequest("Petya", "PM"), new UserResponse("", "Petya", "PM", "")}
        };
    }

    @Test(dataProvider = "entityDataProvider")
    public void postUserRequestEntityDrivenTestingTest(UserRequest request, UserResponse response) {
        UserResponse responseBody = given()
                .spec(rqSpec)
                .contentType(ContentType.JSON)
                .body(request)
                .when()
                .post(Endpoints.USERS_ENDPOINT)
                .as(UserResponse.class);

        System.out.println(responseBody);

        assertThat(responseBody.getJob(), equalTo(request.getJob()));
        assertThat(responseBody.getName(), equalTo(request.getName()));

        assertThat(responseBody, samePropertyValuesAs(response, "createDate", "id"));

//                .then()
//                .spec(rsSpec)
//                .body("name", equalTo("morpheus"))
//                .body("job", equalTo("leader"))
//                .body("id", notNullValue())
//                .body("createdAt", notNullValue());
    }
}
