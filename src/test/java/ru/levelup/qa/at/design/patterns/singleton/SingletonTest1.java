package ru.levelup.qa.at.design.patterns.singleton;

import org.testng.annotations.BeforeSuite;
import org.testng.annotations.Test;

public class SingletonTest1 extends BaseSingletonTest {

    @BeforeSuite
    public void setUp() {
        System.out.println("SingletonTest1 setUp method");
        exampleSingleton = Singleton.getInstance(this.getClass().getName());
    }

    @Test
    public void test1() {
        System.out.println("SingletonTest1.test1");
        System.out.println("Singleton value = " + exampleSingleton.getValue());
        System.out.println("=====");
    }


    @Test
    public void test2() {
        exampleSingleton = Singleton.getInstance("SingletonTest1.test2");
        System.out.println("SingletonTest1.test2");
        System.out.println("Singleton value = " + exampleSingleton.getValue());
        System.out.println("=====");
    }
}
