package ru.levelup.qa.at.design.patterns.singleton;

import org.testng.annotations.Test;

public class SingletonTest2 extends BaseSingletonTest {

    @Test
    public void test1() {
        System.out.println("SingletonTest2.test1");
        System.out.println("Singleton value = " + exampleSingleton.getValue());
        System.out.println("=====");
    }


    @Test
    public void test2() {
        exampleSingleton = Singleton.getInstance(this.getClass().getCanonicalName());
        System.out.println("SingletonTest2.test2");
        System.out.println("Singleton value = " + exampleSingleton.getValue());
        System.out.println("=====");
    }
}
