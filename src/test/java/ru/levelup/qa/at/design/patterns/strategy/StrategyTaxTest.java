package ru.levelup.qa.at.design.patterns.strategy;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import static org.testng.Assert.assertEquals;

public class StrategyTaxTest {

    @DataProvider
    public Object[][] dataProvider() {
        return new Object[][]{
                {new RussianTaxCalculator(), 1000, 130},
                {new RussianTaxCalculator(), 5000, 650},
                {new GermanyTaxCalculator(), 1000, 100},
                {new GermanyTaxCalculator(), 10000, 2200},
                {new GermanyTaxCalculator(), 20000, 6600}
        };
    }

    @Test(dataProvider = "dataProvider")
    public void testName(TaxCalculator calculator, double sum, double expected) {
        TaxResolver resolver = new TaxResolverImpl(calculator);
        System.out.println(String.format("'%s' results is '%s'", calculator.getClass().getSimpleName(), calculator.calculateTax(sum)));
        assertEquals(resolver.calculateTax(sum), expected, 0.00000001);
    }
}
