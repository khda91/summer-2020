package ru.levelup.qa.at.design.patterns.builder;

import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

public class BuilderTest {

    @DataProvider
    public Object[][] dataProvider() {
        return new Object[][]{
                {
                        Person
                                .builder()
                                .firstName("Dima")
                                .dateOfBirth("01-01-2020")
                                .build()
                },
                {
                        Person
                                .builder()
                                .passportSeries(4003)
                                .passportNumber(333444)
                                .secondName("Petrov")
                                .build()
                }
        };
    }

    @Test(dataProvider = "dataProvider")
    public void test(Person person) {
        System.out.println(person);
    }
}
