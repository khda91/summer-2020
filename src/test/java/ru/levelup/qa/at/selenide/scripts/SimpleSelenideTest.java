package ru.levelup.qa.at.selenide.scripts;

import com.codeborne.selenide.Condition;
import org.openqa.selenium.By;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.element;
import static com.codeborne.selenide.Selenide.open;

public class SimpleSelenideTest {

    @Test
    public void userBugredLoginTest() {
        open("http://users.bugred.ru/");

        $(By.xpath("//a//span[text()='Войти']")).click();

        $("[name='login']").sendKeys("test.user@email.com");
        $x("//form[contains(@action, 'login')]//input[@name='password']").sendKeys("test");

        element(By.xpath("//input[@value='Авторизоваться']")).click();

        sleep(3000);

        element(".dropdown-toggle").shouldBe(visible);
        $(".dropdown-toggle").shouldHave(text("test user"));

        // Custom condition
        $(".dropdown-toggle1").waitUntil(visible, 10000);
    }

    void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
