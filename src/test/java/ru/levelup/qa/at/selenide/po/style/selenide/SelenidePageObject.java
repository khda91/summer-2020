package ru.levelup.qa.at.selenide.po.style.selenide;

import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static ru.levelup.qa.at.selenide.po.style.selenide.LoginRegistrationPage.authorizationButton;
import static ru.levelup.qa.at.selenide.po.style.selenide.LoginRegistrationPage.passwordTextField;
import static ru.levelup.qa.at.selenide.po.style.selenide.LoginRegistrationPage.usernameTextField;

public class SelenidePageObject {

    @Test
    public void loginTest() {
        LoginRegistrationPage homePage = new HomePage().open().clickEnterButton();

        usernameTextField.sendKeys("test.user@email.ru");
        passwordTextField.sendKeys("test");
        authorizationButton.click();

//        homePage.userButton().shouldHave(text("test name"));
    }

    @Test
    public void loginTest1() {
        LoginRegistrationPage homePage = new HomePage().open().clickEnterButton();

        usernameTextField.sendKeys("test.user@email.ru");
        passwordTextField.sendKeys("test");
        authorizationButton.click();

//        homePage.userButtonShouldHaveText("test name");
    }
}
