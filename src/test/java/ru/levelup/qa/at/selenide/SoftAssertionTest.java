package ru.levelup.qa.at.selenide;

import org.openqa.selenium.By;
import org.testng.annotations.Test;
import org.testng.asserts.SoftAssert;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.element;
import static com.codeborne.selenide.Selenide.open;
import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class SoftAssertionTest {

    @Test
    public void hardAssertionTest() {
        open("http://users.bugred.ru/user/login/index.html");

        assertTrue($("[name='login']").isDisplayed(), "Login Text field was not displayed");
        assertTrue($x("//form[contains(@action, 'login1')]//input[@name='password']").isDisplayed(),
        "Password Text field was not displayed");
        assertTrue(element(By.xpath("//input[@value='Авторизоваться']")).isDisplayed(), "Enter button was not displayed");
    }

    @Test
    public void softAssertionTest() {
        open("http://users.bugred.ru/user/login/index.html");

        SoftAssert sa = new SoftAssert();

        sa.assertTrue($("[name='login1']").isDisplayed(), "Login Text field was not displayed");
        sa.assertTrue($x("//form[contains(@action, 'login')]//input[@name='password']").isDisplayed(),
                "Password Text field was not displayed");
        assertFalse($(By.partialLinkText("Полная документация")).isDisplayed(), "Link ws not displayed");
        sa.assertTrue(element(By.xpath("//input[@value='Авторизоваться1']")).isDisplayed(), "Enter button was not displayed");

        sa.assertAll();
    }
}
