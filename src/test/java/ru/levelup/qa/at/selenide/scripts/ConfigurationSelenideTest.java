package ru.levelup.qa.at.selenide.scripts;

import com.codeborne.selenide.Browsers;
import com.codeborne.selenide.Configuration;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.element;
import static com.codeborne.selenide.Selenide.open;

public class ConfigurationSelenideTest {

    @BeforeMethod
    public void setUp() {
        Configuration.browser = Browsers.FIREFOX;
        Configuration.timeout = 7500;
        Configuration.baseUrl = "http://users.bugred.ru/";
        Configuration.reportsFolder = "target/reports/tests";
        Configuration.startMaximized = true;
        Configuration.headless = true;
    }

    @Test
    public void userBugredLoginTest() {
        open();

        $(By.xpath("//a//span[text()='Войти']")).click();

        $("[name='login']").sendKeys("test.user@email.com");
        $x("//form[contains(@action, 'login')]//input[@name='password']").sendKeys("test");

        element(By.xpath("//input[@value='Авторизоваться']")).click();

        sleep(3000);

        element(".dropdown-toggle").shouldBe(visible);
        $(".dropdown-toggle").shouldHave(text("test user"));
    }

    @Test
    public void userBugredLoginTest1() {
        open("user/login/index.html");

        $(By.xpath("//a//span[text()='Войти']")).click();

        $("[name='login']").sendKeys("test.user@email.com");
        $x("//form[contains(@action, 'login')]//input[@name='password']").sendKeys("test");

        element(By.xpath("//input[@value='Авторизоваться']")).click();

        sleep(3000);

        element(".dropdown-toggle").shouldBe(visible);
        $(".dropdown-toggle").shouldHave(text("test user1"));
    }

    void sleep(long timeout) {
        try {
            Thread.sleep(timeout);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
