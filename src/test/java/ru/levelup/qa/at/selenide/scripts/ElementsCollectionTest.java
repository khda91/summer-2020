package ru.levelup.qa.at.selenide.scripts;

import org.testng.annotations.Test;

import static com.codeborne.selenide.CollectionCondition.sizeGreaterThan;
import static com.codeborne.selenide.CollectionCondition.textsInAnyOrder;
import static com.codeborne.selenide.Selenide.$$;
import static com.codeborne.selenide.Selenide.elements;
import static com.codeborne.selenide.Selenide.open;
import static org.openqa.selenium.By.xpath;

public class ElementsCollectionTest {

    @Test
    public void elementsCollectionTest() {
        open("http://users.bugred.ru/");

        $$(xpath("//table//tbody[@class='ajax_load_row']//td[1]")).shouldHave(sizeGreaterThan(5));
        elements(xpath("//table//tbody[@class='ajax_load_row']//td[1]"))
                .shouldHave(textsInAnyOrder("anyutka870_v@inbox.ru",
                        "anyutka4_v@inbox.ru", "lasr@eedd.com"));
    }
}
