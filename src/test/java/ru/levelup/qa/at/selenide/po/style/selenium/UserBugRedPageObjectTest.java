package ru.levelup.qa.at.selenide.po.style.selenium;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Condition.visible;
import static com.codeborne.selenide.Selenide.closeWebDriver;

public class UserBugRedPageObjectTest {

    private HomePage homePage;

    @BeforeMethod
    public void setUp() {
        homePage = new HomePage()
                .open();
    }

    @Test
    public void userBugRedLoginTest() {
        homePage
                .clickEnterButton()
                .successfulLogin("test.user@email.com", "test")
                .userButton()
                .shouldHave(text("test user"));
    }

    @Test
    public void userBugRedLoginReadabilityTest() {
        LoginRegistrationPage loginRegistrationPage = homePage.clickEnterButton();

        loginRegistrationPage
                .successfulLogin("test.user@email.com", "test")
                .userButton()
                .shouldHave(text("test user"));
    }

    @Test
    public void userBugRedLoginNegativeTest() {
        homePage
                .clickEnterButton()
                .wrongLogin("test.user@email.com", "hygfhdsbvabdskjvbj")
                .authorizationButton()
                .shouldBe(visible);
    }

    @Test
    public void userBugRedLogoutTest() {
        homePage
                .clickEnterButton()
                .successfulLogin("test.user@email.com", "test")
                .clickUserButton()
                .clickExitButton()
                .enterButton()
                .shouldBe(visible);
    }

    @AfterMethod
    public void tearDown() {
        closeWebDriver();
    }

}
