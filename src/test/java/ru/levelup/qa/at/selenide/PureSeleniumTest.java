package ru.levelup.qa.at.selenide;

import org.testng.annotations.Test;

import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.getWebDriver;

public class PureSeleniumTest {

    @Test
    public void test() {
        open();
        getWebDriver().get("http://users.bugred.ru/");

        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            Thread.currentThread().interrupt();
        }

    }
}
