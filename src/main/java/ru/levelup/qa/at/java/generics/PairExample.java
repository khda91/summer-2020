package ru.levelup.qa.at.java.generics;

import ru.levelup.qa.at.java.collections.data.Person;

public class PairExample {

    public static void main(String[] args) {
        Pair<Person, Person> pair = new Pair<>(
                new Person("Stella", 24),
                new Person("Anatoly", 30));

        System.out.println(pair);
        System.out.println(pair.getFirst());
        System.out.println(pair.getSecond());
        System.out.println(pair.getFirst().getName());
    }
}
