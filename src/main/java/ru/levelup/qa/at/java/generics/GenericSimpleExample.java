package ru.levelup.qa.at.java.generics;

import ru.levelup.qa.at.java.collections.data.Person;

import java.util.ArrayList;

public class GenericSimpleExample {

    public static void main(String[] args) {
        ArrayList arrayList = new ArrayList();
        arrayList.add("Some string");
        arrayList.add(10);
        arrayList.add(556.778D);
        arrayList.add(new Person("Mama", 30));

        for (Object o : arrayList) {
            if (o instanceof String) {
                System.out.println("string");
            } else if (o instanceof Integer) {
                System.out.println("int");
            } else if (o instanceof Double) {
                System.out.println("double");
            } else if (o instanceof Person) {
                System.out.println("Person");
                Person p = (Person) o;
                System.out.println(p.getName());
            } else {
                System.out.println("something else");
            }
        }
    }
}
