package ru.levelup.qa.at.java.operations;

public class BreakContinueReturn {

    public static void main(String[] args) {
//        breakOperator();
//        continueOperator();
//        System.out.println(returnOperator());
        String[] strings = {"hello", "say", "sdaaa", "jjhjhhhsdi", "schedule"};
        for (String string : strings) {
            returnOperatorWithVoid(string);
        }
    }

    static void returnOperatorWithVoid(String str) {
        if (str.contains("h")) {
            System.out.println(str + " contains 'h' letter");
            return;
        }
        System.out.println(str.replaceAll("s", "6"));
    }

    static void breakOperator() {
        int[] a = {1, 4, 2, 7, 8, 9, 10};
        int b = 0;
        for (int i : a) {
            b++;
            if (i == 7) {
                break;
            }
        }
        System.out.println("b = " + b);
    }

    static void continueOperator() {
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i : a) {
            if (i % 2 == 0) {
                continue;
            }
            System.out.println(i);
        }
    }

    static int returnOperator() {
        int[] a = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
        for (int i : a) {
            if (i == 7) {
                return i;
            }
        }
        return 0;
    }
}
