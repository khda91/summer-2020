package ru.levelup.qa.at.java.collision;

public class Person {

    private String name;
    protected long age;

    public Person(String name) {
        this.name = name;
        this.age = System.currentTimeMillis();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public long getAge() {
        return (System.currentTimeMillis() - age) / 1000;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        Person person = (Person) o;

        if (age != person.age) return false;
        return name != null ? name.equals(person.name) : person.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + (int) (age ^ (age >>> 32));
        return result;
    }
}
