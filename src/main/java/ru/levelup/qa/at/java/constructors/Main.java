package ru.levelup.qa.at.java.constructors;

public class Main {

    public static void main(String[] args) {
        Point p1 = new Point();
        Point p2 = new Point(5.55, 10.8);

        System.out.println("p1: " + p1.getX() + " || " + p1.getY());
        System.out.println("p2: " + p2.getX() + " || " + p2.getY());
    }

}
