package ru.levelup.qa.at.java.collections.maps;

import ru.levelup.qa.at.java.collections.data.Person;

import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;

public class MapExample {

    public static void main(String[] args) {
        System.out.println("=========================");
        System.out.println("HashMap");
        hashMapExample();
        System.out.println("=========================");

        System.out.println("=========================");
        System.out.println("LinkedHashMap");
        linkedHashMapExample();
        System.out.println("=========================");

        System.out.println("=========================");
        System.out.println("TreeMap");
        treeMapExample();
        System.out.println("=========================");

        System.out.println("=========================");
        System.out.println("HashMap with objects");
        hashMapWithObjectsExample();
        System.out.println("=========================");
    }

    static void hashMapWithObjectsExample() {
        Map<Person, Person> wifeAndHusbandMap = new HashMap<>();
        wifeAndHusbandMap.put(new Person("Stella", 24), new Person("John", 26));
        wifeAndHusbandMap.put(new Person("Tatiana", 23), new Person("Evgeny", 30));
        wifeAndHusbandMap.put(new Person("Mariia", 30), new Person("Pavel", 26));
        wifeAndHusbandMap.put(new Person("Stella", 24), new Person("Alexander", 40));
        wifeAndHusbandMap.put(new Person("Agata", 20), new Person("Elton", 50));
        wifeAndHusbandMap.put(new Person("Margarita", 30), new Person("Anatoly", 32));

        System.out.println("add objects to the map");
        for (Person person : wifeAndHusbandMap.keySet()) {
            System.out.println(String.format("%s is %d years old. She has husband %s %d years old.",
                    person.getName(), person.getAge(),
                    wifeAndHusbandMap.get(person).getName(), wifeAndHusbandMap.get(person).getAge()));
        }
        System.out.println();
    }

    static void treeMapExample() {
        Map<Person, Person> wifeAndHusbandMap = new TreeMap<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                if (o1.getName().compareTo(o2.getName()) == 0) {
                    if (o1.getAge() < o2.getAge()) {
                        return 1;
                    } else if (o1.getAge() == o2.getAge()) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
                return o1.getName().compareTo(o2.getName()) * -1;
            }
        });
        wifeAndHusbandMap.put(new Person("Stella", 24), new Person("John", 26));
        wifeAndHusbandMap.put(new Person("Tatiana", 23), new Person("Evgeny", 30));
        wifeAndHusbandMap.put(new Person("Mariia", 30), new Person("Pavel", 26));
        wifeAndHusbandMap.put(new Person("Stella", 24), new Person("Alexander", 40));
        wifeAndHusbandMap.put(new Person("Agata", 20), new Person("Elton", 50));
        wifeAndHusbandMap.put(new Person("Margarita", 30), new Person("Anatoly", 32));

        System.out.println("add objects to the map");
        for (Person person : wifeAndHusbandMap.keySet()) {
            System.out.println(String.format("%s is %d years old. She has husband %s %d years old.",
                    person.getName(), person.getAge(),
                    wifeAndHusbandMap.get(person).getName(), wifeAndHusbandMap.get(person).getAge()));
        }
        System.out.println();
    }

    static void linkedHashMapExample() {
        Map<String, Integer> stringMap = new LinkedHashMap<>();
        stringMap.put("Vova", 20);
        stringMap.put("Papa", 50);
        stringMap.put("Mike", 12);
        stringMap.put("Vova", 21);
        stringMap.put("Michele", 24);
        stringMap.put("Vova", 21);
        stringMap.put("Vlad", 20);

        System.out.println("Add value to hashMap");
        System.out.println(stringMap);
        System.out.println();

        System.out.println("Get value for map");
        System.out.println(String.format("Mile is %s years old.", stringMap.get("Mike")));
        System.out.println();

        System.out.println("Run through the map by key set");
        for (String key : stringMap.keySet()) {
            System.out.println(String.format("For '%s' key value is '%s'", key, stringMap.get(key)));
        }
        System.out.println();

        System.out.println("Run through the map by entry table");
        Set<Map.Entry<String, Integer>> entries = stringMap.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println(String.format("For '%s' key value is '%s'", entry.getKey(), entry.getValue()));
        }
        System.out.println();

        System.out.println("delete value from map");
        stringMap.remove("Papa");
        System.out.println(stringMap);
        System.out.println();
    }

    static void hashMapExample() {
        Map<String, Integer> stringMap = new HashMap<>();
        stringMap.put("Vova", 20);
        stringMap.put("Papa", 50);
        stringMap.put("Mike", 12);
        stringMap.put("Vova", 21);
        stringMap.put("Michele", 24);
        stringMap.put("Vova", 21);
        stringMap.put("Vlad", 20);

        System.out.println("Add value to hashMap");
        System.out.println(stringMap);
        System.out.println();

        System.out.println("Get value for map");
        System.out.println(String.format("Mile is %s years old.", stringMap.get("Mike")));
        System.out.println();

        System.out.println("Run through the map by key set");
        for (String key : stringMap.keySet()) {
            System.out.println(String.format("For '%s' key value is '%s'", key, stringMap.get(key)));
        }
        System.out.println();

        System.out.println("Run through the map by entry table");
        Set<Map.Entry<String, Integer>> entries = stringMap.entrySet();
        for (Map.Entry<String, Integer> entry : entries) {
            System.out.println(String.format("For '%s' key value is '%s'", entry.getKey(), entry.getValue()));
        }
        System.out.println();

        System.out.println("delete value from map");
        stringMap.remove("Papa");
        System.out.println(stringMap);
        System.out.println();
    }
}
