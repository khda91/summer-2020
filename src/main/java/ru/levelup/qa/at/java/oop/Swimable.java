package ru.levelup.qa.at.java.oop;

public interface Swimable {

     default void swim() {
         System.out.println("Swwwiiiiiiimmmm");
     }
}
