package ru.levelup.qa.at.java.collections.list;

import ru.levelup.qa.at.java.collections.data.Person;

import java.util.ArrayList;
import java.util.List;

public class ListExample {

    public static void main(String[] args) {
        List<Person> personList = new ArrayList<>();
        personList.add(new Person("Stella", 24));
        personList.add(new Person("Anatoly", 30));
        personList.add(new Person("Vova", 233));
        personList.add(new Person("Nick", 26));
        personList.add(new Person("Stella", 24));
        personList.add(new Person("Margo", 23));

        System.out.println("Add to list");
        System.out.println(personList);
        System.out.println();

        personList.add(new Person("Stella", 24));
        personList.add(new Person("Anatoly", 30));
        personList.add(new Person("Vova", 233));
        personList.add(new Person("Nick", 26));
        personList.add(new Person("Stella", 24));
        personList.add(new Person("Margo", 23));

        System.out.println("Add more to list");
        System.out.println(personList);
        System.out.println();

        System.out.println("Get element from list");
        System.out.println(personList.get(10));
        System.out.println();

        System.out.println("Delete element from list");
        personList.remove(10);
        personList.remove(new Person("Margo", 23));
        System.out.println(personList);
        System.out.println();

        List<List<String>> lst = new ArrayList<>();
        String[][] strs = new String[2][2];
    }
}
