package ru.levelup.qa.at.java.oop;

public class Dwarf extends Person implements Runnable {

    private double height;
    private double beardLength;

    public Dwarf(String name) {
        super(name);
        age *= 4;
        this.height = 8.2;
        this.beardLength = 1.3;
        this.strength = 10;
    }

    public Dwarf(String name, double height, double beardLength) {
        super(name);
        this.height = height;
        this.beardLength = beardLength;
    }

    public Dwarf(String name, int strength) {
        super(name);
        this.strength = strength;
    }

    public double getHeight() {
        return height;
    }

    public double getBeardLength() {
        return beardLength;
    }

    public void setBeardLength(double beardLength) {
        this.beardLength = beardLength;
    }

    @Override
    public void jump() {
        System.out.println(getName() + " is jumping low");
    }

    @Override
    public void move() {
        distance += 3;
    }

    @Override
    public String toString() {
        return "Dwarf{" +
                "height=" + height +
                ", beardLength=" + beardLength +
                ", age=" + age +
                '}';
    }

    @Override
    public void run() {

    }
}
