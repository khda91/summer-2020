package ru.levelup.qa.at.java.operations;

public class Operations {

    public static void main(String[] args) {
        // postfix plus
        int a = 5;
        System.out.println("a = " + (a++));
        int b = 8;
        System.out.println("b = " + (--b));
        System.out.println("a = " + (a));

        int c = 5;
        System.out.println("c = " + (c++));
        int d = c /*c = 6*/ + 10; // d = 16
        assert d == 15;
    }
}
