package ru.levelup.qa.at.java.arrays;

public class Arrays {

    public static void main(String[] args) {
        String[] strings = {"mama", "papa", "roma", "vova"};

        for (String iter: strings) {
            System.out.println(iter.toUpperCase().replaceAll("A", "8"));
        }

        // Modify existing values
        strings[2] = "Nick";
        System.out.println("================");
        for (String iter: strings) {
            System.out.println(iter.toUpperCase().replaceAll("A", "8"));
        }

        // extend size of arrays
        String[] ss = new String[5];
        for (int i = 0; i < strings.length; i++) {
            ss[i] = strings[i];
        }
        ss[4] = "jjjjjj";
        strings = null;
        System.out.println("================");
        for (String iter: ss) {
            System.out.println(iter.toUpperCase().replaceAll("A", "8"));
        }
    }
}
