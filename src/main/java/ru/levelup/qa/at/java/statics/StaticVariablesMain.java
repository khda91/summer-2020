package ru.levelup.qa.at.java.statics;

public class StaticVariablesMain {

    public static void main(String[] args) {
        System.out.println("o = " + Point.o);
        Point p1 = new Point();
        Point p2 = new Point(5.55, 10.8);

        Point.o = 7777;
        System.out.println("p1: " + p1.getX() + " || " + p1.getY());
        System.out.println("p2: " + p2.getX() + " || " + p2.getY());
        System.out.println("o = " + p2.o);
    }
}
