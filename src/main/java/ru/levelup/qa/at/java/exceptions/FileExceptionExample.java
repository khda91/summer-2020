package ru.levelup.qa.at.java.exceptions;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class FileExceptionExample {

    /*
        программа принимает на воход файл со списком сторок
        зазделитель строк новая строка
        разделитель слов ;
        написать метод который принимает на вход список строк
        заменяет все будквы P на 10, переводит в верехний регистер,
        выводит только уникальные значения
     */

    public static void main(String[] args) {
        File file = new File("test.txt");
        FileExceptionExample fee = new FileExceptionExample();
        List<String> strings = null;
        try {
            strings = fee.readFromFile(file);
        } catch (IOException e) {
            e.printStackTrace();
        }

        strings.add(null);

        List<List<String>> lists = fee.parseData(strings);
        lists.add(null);
        lists.add(new ArrayList<>(Arrays.asList("jjkoppplsd", null)));
        for (List<String> list : lists) {
            if (list != null && !list.isEmpty()) {
                System.out.println(fee.formatString(list));
            }
        }
    }

    Set<String> formatString(List<String> list) {
        Set<String> result = new HashSet<>();
        for (String s : list) {
            if (s != null && !s.isEmpty()) {
                String p = s.trim().toUpperCase().replaceAll("P", "10");
                result.add(p);
            }
        }
        return result;
    }

    List<List<String>> parseData(List<String> str) {
        if (str == null || str.isEmpty()) {
            throw new IllegalArgumentException("Parameter should not be null or empty");
        }
        List<List<String>> result = new ArrayList<>();
        for (String s : str) {
            if (s != null) {
                result.add(Arrays.asList(s.split(";")));
            }
        }
        return result;
    }

    List<String> readFromFile(File file) throws IOException {
        List<String> result = null;
        FileReader fr = null;
        BufferedReader br = null;
        try {
            fr = new FileReader(file);
            br = new BufferedReader(fr);
            result = new ArrayList<>();
            String line;
            while ((line = br.readLine()) != null) {
                result.add(line);
            }
        } catch (FileNotFoundException e) {
            System.err.println("Unable to find file: " + file.getAbsolutePath() + ". Error " +
                    new MyExampleException("Example", e));
        } catch (IOException e) {
            System.err.println("Something went wrong. Error " + e);
        } finally {
            if (fr != null) {
                fr.close();
            }

            if (br != null) {
                try {
                    br.close();
                } catch (IOException e) {
                    System.err.println(e);
                }
            }
        }

        return result;
    }
}
