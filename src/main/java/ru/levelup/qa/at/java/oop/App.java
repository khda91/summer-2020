package ru.levelup.qa.at.java.oop;

public class App {

    public static void main(String... args) {
        Dwarf anatoly = new Dwarf("Anatoly", 20D, 10D);
        LightElf nikita = new LightElf("Nikita", 4);
        DarkElf kostyan = new DarkElf("Kostyan");
        Dwarf tolyan = new Dwarf("Tolyan", 40);

        System.out.println("Persons created");
        System.out.println(anatoly + " was create");
        System.out.println(nikita.getName() + " was created");

        nikita.jump();
        anatoly.jump();
        kostyan.jump();

        System.out.println();

        Swimable[] swimables = {nikita, kostyan};
        for (Swimable swimable : swimables) {
            swimable.swim();
        }

        System.out.println();
        kostyan.fight(anatoly);
        kostyan.fight(tolyan);

        kostyan.fight(anatoly, tolyan);
    }
}
