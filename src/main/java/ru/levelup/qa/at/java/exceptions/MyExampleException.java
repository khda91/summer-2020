package ru.levelup.qa.at.java.exceptions;

public class MyExampleException extends RuntimeException {

    public MyExampleException() {
    }

    public MyExampleException(String message) {
        super(message);
    }

    public MyExampleException(String message, Throwable cause) {
        super(message, cause);
    }

    public MyExampleException(Throwable cause) {
        super(cause);
    }

    public MyExampleException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
