package ru.levelup.qa.at.java.constructors.shapes;

public class Circle extends Shape {

    private double radius;

    public Circle() {
        System.out.println("Circle constructor invoke");
    }

    public double getRadius() {
        return radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }
}
