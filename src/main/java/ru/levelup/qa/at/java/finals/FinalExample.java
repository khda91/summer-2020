package ru.levelup.qa.at.java.finals;

public class FinalExample {

    public static void main(String[] args) {
        // primitives
//        final int a = 3;
//        a = a + 9;

        // linked
        final MySampleClass msc = new MySampleClass(10);
        System.out.println(msc.getA());
        System.out.println("Change state");
        msc.setA(39);
        System.out.println(msc.getA());
//        msc = new MySampleClass(49);
    }
}

class MySampleClass {
    private int a;

    public MySampleClass(int a) {
        this.a = a;
    }

    public int getA() {
        return a;
    }

    public void setA(int a) {
        this.a = a;
    }
}
