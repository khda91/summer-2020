package ru.levelup.qa.at.java.constructors.shapes;

public class Rectangle extends Shape {

    private double width;
    private double height;

    public Rectangle() {
        System.out.println("Rectangle constructor invoke");
    }
}
