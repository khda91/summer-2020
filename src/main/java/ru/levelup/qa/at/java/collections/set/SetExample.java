package ru.levelup.qa.at.java.collections.set;

import ru.levelup.qa.at.java.collections.data.Person;
import ru.levelup.qa.at.java.collections.data.PersonComparable;

import java.util.Arrays;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Set;
import java.util.TreeSet;

public class SetExample {

    public static void main(String[] args) {
        System.out.println("=========================");
        System.out.println("HashSet");
        hashSetExample();
        System.out.println("=========================");

        System.out.println("=========================");
        System.out.println("LinkedHashSet");
        linkedHashSetExample();
        System.out.println("=========================");

        System.out.println("=========================");
        System.out.println("TreeSet");
        treeSetExample();
        System.out.println("=========================");

        System.out.println("=========================");
        System.out.println("HashSet With Person objects");
        hashSetWithPersonObjectsExample();
        System.out.println("=========================");

        System.out.println("=========================");
        System.out.println("TreeSet With Person objects and Comparator");
        treeSetWithPersonObjectsAndComparatorExample();
        System.out.println("=========================");

        System.out.println("=========================");
        System.out.println("TreeSet With Person objects and Comparable");
        treeSetWithPersonObjectsAndComparableExample();
        System.out.println("=========================");
    }

    static void treeSetWithPersonObjectsAndComparableExample() {
        Set<PersonComparable> personSet = new TreeSet<>();

        PersonComparable lance = new PersonComparable("Lance", 43);
        personSet.addAll(Arrays.asList(new PersonComparable("Vova", 20),
                lance, new PersonComparable("Mike", 50),
                new PersonComparable("Vova", 21), new PersonComparable("Vova", 20),
                new PersonComparable("Michel", 20)));

        System.out.println("add values to person  tree set");
        printSetPersonComparable(personSet);

        System.out.println();

        personSet.remove(lance);
        System.out.println("delete value from person tree set");
        printSetPersonComparable(personSet);
    }

    static void treeSetWithPersonObjectsAndComparatorExample() {
        Set<Person> personSet = new TreeSet<>(new Comparator<Person>() {
            @Override
            public int compare(Person o1, Person o2) {
                if (o1.getName().compareTo(o2.getName()) == 0) {
                    if (o1.getAge() < o2.getAge()) {
                        return 1;
                    } else if (o1.getAge() == o2.getAge()) {
                        return 0;
                    } else {
                        return -1;
                    }
                }
                return o1.getName().compareTo(o2.getName()) * -1;
            }
        });
        Person lance = new Person("Lance", 43);
        personSet.addAll(Arrays.asList(new Person("Vova", 20),
                lance, new Person("Mike", 50),
                new Person("Vova", 21), new Person("Vova", 20),
                new Person("Michel", 20)));

        System.out.println("add values to person  tree set");
        printSetPerson(personSet);

        System.out.println();

        personSet.remove(lance);
        System.out.println("delete value from person tree set");
        printSetPerson(personSet);
    }

    static void hashSetWithPersonObjectsExample() {
        Set<Person> personSet = new HashSet<>();
        Person lance = new Person("Lance", 43);
        personSet.addAll(Arrays.asList(new Person("Vova", 20),
                lance, new Person("Mike", 50),
                new Person("Vova", 21), new Person("Vova", 20)));

        System.out.println("add values to person set");
        printSetPerson(personSet);

        System.out.println();

        personSet.remove(lance);
        System.out.println("delete value from person set");
        printSetPerson(personSet);
    }

    static void hashSetExample() {
        String baba = "babushka";

        Set<String> stringSet = new HashSet<>();
        stringSet.add("mama");
        stringSet.add("papa");
        stringSet.add("vova");
        stringSet.add("momo");
        stringSet.add("mama");
        stringSet.add("momo");
        stringSet.add(baba);
        System.out.println("add values to string set");
        printSetString(stringSet);

        System.out.println();

        stringSet.remove("momo");
        System.out.println("delete value from string set");
        printSetString(stringSet);

//        // Полохой код! Так не делать!
//        Set<Object> objectSet = new HashSet<>();
//        objectSet.add("mama");
//        objectSet.add("papa");
//        objectSet.add("vova");
//        objectSet.add("momo");
//        objectSet.add("mama");
//        objectSet.add("momo");
//        objectSet.add(new Object());
//        objectSet.addAll(Arrays.asList(12,23412,324213,823.888D));
//        System.out.println("add values to object set");
//        printSetObject(objectSet);
//        System.out.println("print string");
//        Iterator iterator = objectSet.iterator();
//        while (iterator.hasNext()) {
//            printString((String) iterator.next());
//        }
//        System.out.println();
    }

    static void linkedHashSetExample() {
        String baba = "babushka";

        Set<String> stringSet = new LinkedHashSet<>();
        stringSet.add("mama");
        stringSet.add("papa");
        stringSet.add("vova");
        stringSet.add("momo");
        stringSet.add("mama");
        stringSet.add("momo");
        stringSet.add(baba);
        System.out.println("add values to string linked set");
        printSetString(stringSet);

        System.out.println();

        stringSet.remove("momo");
        System.out.println("delete value from string linked set");
        printSetString(stringSet);
    }

    static void treeSetExample() {
        String baba = "babushka";

        Set<String> stringSet = new TreeSet<>();
        stringSet.add("mama");
        stringSet.add("papa");
        stringSet.add("vova");
        stringSet.add("momo");
        stringSet.add("mama");
        stringSet.add("momo");
        stringSet.add(baba);
        System.out.println("add values to string linked set");
        printSetString(stringSet);

        System.out.println();

        stringSet.remove("momo");
        System.out.println("delete value from string linked set");
        printSetString(stringSet);
    }

    static void printString(String str) {
        System.out.print(str + " ");
    }

    static void printSetString(Set<String> set) {
        Iterator<String> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }

    static void printSetPerson(Set<Person> set) {
        Iterator<Person> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " || ");
        }
        System.out.println();
    }

    static void printSetPersonComparable(Set<PersonComparable> set) {
        Iterator<PersonComparable> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " || ");
        }
        System.out.println();
    }

    static void printSetObject(Set<Object> set) {
        Iterator<Object> iterator = set.iterator();
        while (iterator.hasNext()) {
            System.out.print(iterator.next() + " ");
        }
        System.out.println();
    }
}
