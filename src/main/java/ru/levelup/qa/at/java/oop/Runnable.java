package ru.levelup.qa.at.java.oop;

public interface Runnable {

    void run();
}
