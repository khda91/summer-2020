package ru.levelup.qa.at.java.overload;

public class OverloadExample {

    public static void main(String[] args) {
        System.out.println(sum(1, 4));
        System.out.println(sum(1D, 6D));
    }

    static int sum(int a, int b) {
        return a + b;
    }

    static int sum(double a, double b) {
        return (int) (a + b);
    }

    static double sum(long a, long b) {
        return a + b;
    }
}
