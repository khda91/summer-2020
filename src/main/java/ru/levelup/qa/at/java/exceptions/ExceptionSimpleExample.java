package ru.levelup.qa.at.java.exceptions;

public class ExceptionSimpleExample {

    public static void main(String[] args) {
        check("20");
        check("sas");
    }

    private static void check(String str) {
        System.out.println(Integer.parseInt(str));
    }
}
