package ru.levelup.qa.at.java.oop;

public class DarkElf extends Elf {

    public DarkElf(String name) {
        super(name);
        this.strength = 20;
    }

    @Override
    public void jump() {
        System.out.println(getName() + " is very very very high jump");
    }

    @Override
    public void swim() {
        System.out.println(getName() + " cannot swim, but can walk on the water");
    }

    @Override
    public void fight(Person... person) { // public void fight(Person[] person)
        for (Person person1 : person) {
            this.fight(person1);
        }
    }
}
