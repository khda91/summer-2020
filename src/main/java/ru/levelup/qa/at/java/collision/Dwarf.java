package ru.levelup.qa.at.java.collision;

public class Dwarf extends Person {

    private double height;
    private double beardLength;

    public Dwarf(String name) {
        super(name);
        age *= 4;
        this.height = 8.2;
        this.beardLength = 1.3;
    }

    public Dwarf(String name, double height, double beardLength) {
        super(name);
        this.height = height;
        this.beardLength = beardLength;
    }

    public double getHeight() {
        return height;
    }

    public double getBeardLength() {
        return beardLength;
    }

    public void setBeardLength(double beardLength) {
        this.beardLength = beardLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Dwarf dwarf = (Dwarf) o;

        if (Double.compare(dwarf.height, height) != 0) return false;
        return Double.compare(dwarf.beardLength, beardLength) == 0;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(height);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        temp = Double.doubleToLongBits(beardLength);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public String toString() {
        return "Dwarf{" +
                "height=" + height +
                ", beardLength=" + beardLength +
                ", age=" + age +
                '}';
    }
}
