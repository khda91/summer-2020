package ru.levelup.qa.at.java.oop;

public interface Fightable {

    String WINS_LABEL = "%s vs %s. %s wins";

    void fight(Person person);

    void fight(Person... person);
}
