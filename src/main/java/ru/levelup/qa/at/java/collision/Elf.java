package ru.levelup.qa.at.java.collision;

public class Elf extends Person {

    private double earLength;

    public Elf(String name) {
        super(name);
        super.age *= 2;
        this.earLength = 1.5;
    }

    public Elf(String name, double earLength) {
        super(name); // конструктор родительского класса
        this.earLength = earLength;
    }

    public double getEarLength() {
        return earLength;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        Elf elf = (Elf) o;

        return Double.compare(elf.earLength, earLength) == 0;
    }

    @Override
    public int hashCode() {
        return 123;
    }
}
