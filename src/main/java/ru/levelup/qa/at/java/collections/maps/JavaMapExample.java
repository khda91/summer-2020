package ru.levelup.qa.at.java.collections.maps;

import java.util.Map;

public class JavaMapExample {

    public static void main(String[] args) {
        Map<String, String> getenv = System.getenv();

        for (Map.Entry<String, String> entry : getenv.entrySet()) {
            System.out.println(String.format("%s = %s", entry.getKey(), entry.getValue()));
        }
    }
}
