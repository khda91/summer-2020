package ru.levelup.qa.at.java.constructors.shapes;

public abstract class Shape {

    protected Shape() {
        System.out.println("Shape constructor invoke");
    }
}
