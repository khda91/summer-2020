package ru.levelup.qa.at.java.collections.data;

public class PersonComparable implements Comparable<PersonComparable> {

    private String name;
    private int age;

    public PersonComparable(String name, int age) {
        this.name = name;
        this.age = age;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        PersonComparable person = (PersonComparable) o;

        if (age != person.age) return false;
        return name != null ? name.equals(person.name) : person.name == null;
    }

    @Override
    public int hashCode() {
        int result = name != null ? name.hashCode() : 0;
        result = 31 * result + age;
        return result;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", age=" + age +
                '}';
    }

    @Override
    public int compareTo(PersonComparable o) {
        if (this.getName().compareTo(o.getName()) == 0) {
            if (this.getAge() < o.getAge()) {
                return -1;
            } else if (this.getAge() == o.getAge()) {
                return 0;
            } else {
                return 1;
            }
        }
        return this.getName().compareTo(o.getName());
    }
}
