package ru.levelup.qa.at.java.collision;

public class App {

    public static void main(String[] args) {
        Dwarf anatoly = new Dwarf("Anatoly", 20D, 10D);
        Dwarf nikolay = new Dwarf("Nikolay", 20D, 10D);
        Dwarf tolyan = new Dwarf("Anatoly", 20D, 10D);

        Elf nikita = new Elf("Nikita");
        Elf vova = new Elf("Vladimir");

        System.out.println("Is Anatoly equal to Nikolay: " + anatoly.equals(nikolay));
        System.out.println("Is Anatoly == Nikolay: " + (anatoly == nikolay));
        System.out.println("========");
        System.out.println("Is Anatoly equal to Tolyan: " + anatoly.equals(tolyan));
        System.out.println("Is Anatoly == Tolyan: " + (anatoly == tolyan));
        System.out.println();
        System.out.println("========");
        System.out.println("Is Nikita equal to Vladimir: " + nikita.equals(vova)); // false
        System.out.println("Is Nikita == Vladimir: " + (nikita == vova)); // true

    }
}
