package ru.levelup.qa.at.java.oop;

public class LightElf extends Elf implements Runnable {

    private double earLength;

    public LightElf(String name) {
        super(name);
        super.age *= 2;
        this.earLength = 1.5;
    }

    public LightElf(String name, double earLength) {
        super(name); // конструктор родительского класса
        this.earLength = earLength;
    }

    public double getEarLength() {
        return earLength;
    }

    @Override
    public void fight(Person person) {
        System.out.println(String.format(WINS_LABEL, getName(), person.getName(),
                this.strength > person.strength ? getName() : person.getName()));
    }

    @Override
    public void fight(Person... person) {
        throw new UnsupportedOperationException("Method does not supported");
    }

    @Override
    public void jump() {
        System.out.println(getName() + " is jumping very high");
    }

    @Override
    public void move() {
        distance += 10;
    }

    @Override
    public void swim() {
        System.out.println(getName() + " can swim");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;

        LightElf elf = (LightElf) o;

        return Double.compare(elf.earLength, earLength) == 0;
    }

    @Override
    public int hashCode() {
        int result = super.hashCode();
        long temp;
        temp = Double.doubleToLongBits(earLength);
        result = 31 * result + (int) (temp ^ (temp >>> 32));
        return result;
    }

    @Override
    public void run() {

    }
}
