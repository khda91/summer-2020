package ru.levelup.qa.at.selenide.po.style.selenide;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class HomePage {

    private static final String URL = "http://users.bugred.ru/";

    public HomePage open() {
        return Selenide.open(URL, HomePage.class);
    }

    public LoginRegistrationPage clickEnterButton() {
        $x("//a//span[text()='Войти']").click();
        return page(LoginRegistrationPage.class);
    }

//    public void clickEnterButton() {
//        $x("//a//span[text()='Войти']").click();
//    }

    public SelenideElement userButton() {
        return $(".dropdown-toggle");
    }

    public void userButtonShouldHaveText(String text) {
        $(".dropdown-toggle").shouldHave(text(text));
    }
}
