package ru.levelup.qa.at.selenide.po.style.selenium;

import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class LoginRegistrationPage {

    @FindBy(name = "login")
    private SelenideElement usernameTextField;

    @FindBy(xpath = "//form[contains(@action, 'login')]//input[@name='password']")
    private SelenideElement passwordTextField;

    @FindBy(xpath = "//input[@value='Авторизоваться']")
    private SelenideElement authorizationButton;

    public HomePage successfulLogin(String username, String password) {
        login(username, password);
        return page(HomePage.class);
    }

    public LoginRegistrationPage wrongLogin(String username, String password) {
        login(username, password);
        return page(this);
    }

    private void login(String username, String password) {
        usernameTextField.sendKeys(username);
        passwordTextField.sendKeys(password);
        authorizationButton.click();
    }

    public SelenideElement authorizationButton() {
        return authorizationButton;
    }
}
