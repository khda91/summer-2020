package ru.levelup.qa.at.selenide.po.style.selenium;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.support.FindBy;

import static com.codeborne.selenide.Selenide.page;

public class HomePage {

    private static final String URL = "http://users.bugred.ru/";

    @FindBy(xpath = "//a//span[text()='Войти']")
    private SelenideElement enterButton;

    @FindBy(className = "dropdown-toggle")
    private SelenideElement userButton;

    @FindBy(linkText = "Выход")
    private SelenideElement exitButton;

    public HomePage open() {
        Selenide.open(URL);
        return page(this);
    }

    public LoginRegistrationPage clickEnterButton() {
        enterButton.click();
        return page(LoginRegistrationPage.class);
    }

    public SelenideElement enterButton() {
        return enterButton;
    }

    public HomePage clickUserButton() {
        userButton.click();
        return page(this);
    }

    public SelenideElement userButton() {
        return userButton;
    }

    public HomePage clickExitButton() {
        exitButton.click();
        return page(this);
    }
}
