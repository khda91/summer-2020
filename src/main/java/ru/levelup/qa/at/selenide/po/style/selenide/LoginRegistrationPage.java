package ru.levelup.qa.at.selenide.po.style.selenide;

import com.codeborne.selenide.SelenideElement;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class LoginRegistrationPage {

    public static SelenideElement usernameTextField = $("[name='login']");

    public static SelenideElement passwordTextField = $x("//form[contains(@action, 'login')]//input[@name='password']");

    public static SelenideElement authorizationButton = $x("//input[@value='Авторизоваться']");

    public SelenideElement userButton() {
        return $(".dropdown-toggle");
    }

    public void userButtonShouldHaveText(String text) {
        $(".dropdown-toggle").shouldHave(text(text));
    }
}
