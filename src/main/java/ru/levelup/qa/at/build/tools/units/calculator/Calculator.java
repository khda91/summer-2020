package ru.levelup.qa.at.build.tools.units.calculator;

public class Calculator {

    public double add(double a, double b) {
        return a + b;
    }

    public double substract(double a, double b) {
        return a - b;
    }

    public double multiply(double a, double b) {
        return a * b;
    }

    public double divide(double a, double b) {
        if (b == 0.0D) {
            throw new IllegalArgumentException();
        }
        return a / b;
    }

    public long divide(long a, long b) {
        return a / b;
    }
}
