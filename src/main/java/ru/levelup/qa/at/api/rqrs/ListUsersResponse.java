package ru.levelup.qa.at.api.rqrs;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class ListUsersResponse {

    private int page;

    @SerializedName("per_page")
    private int perPage;
    private int total;

    @SerializedName("total_pages")
    private int totalPages;
    private List<User> data;

    public ListUsersResponse() {
    }

    public ListUsersResponse(int page, int perPage, int total, int totalPages, List<User> data) {
        this.page = page;
        this.perPage = perPage;
        this.total = total;
        this.totalPages = totalPages;
        this.data = data;
    }

    public int getPage() {
        return page;
    }

    public int getPerPage() {
        return perPage;
    }

    public int getTotal() {
        return total;
    }

    public int getTotalPages() {
        return totalPages;
    }

    public List<User> getData() {
        return data;
    }

    @Override
    public String toString() {
        return "ListUsersResponse{" +
                "page=" + page +
                ", perPage=" + perPage +
                ", total=" + total +
                ", totalPages=" + totalPages +
                ", data=" + data +
                '}';
    }
}
