package ru.levelup.qa.at.api.rqrs;

import com.google.gson.annotations.SerializedName;

import java.util.Objects;

public class UserResponse {

    private String id;
    private String name;
    private String job;

    @SerializedName("createdAt")
    private String createDate;

    public UserResponse(String id, String name, String job, String createDate) {
        this.id = id;
        this.name = name;
        this.job = job;
        this.createDate = createDate;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getJob() {
        return job;
    }

    public String getCreateDate() {
        return createDate;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        UserResponse that = (UserResponse) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(job, that.job) &&
                Objects.equals(createDate, that.createDate);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, job, createDate);
    }

    @Override
    public String toString() {
        return "UserResponse{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", job='" + job + '\'' +
                ", createDate='" + createDate + '\'' +
                '}';
    }
}
