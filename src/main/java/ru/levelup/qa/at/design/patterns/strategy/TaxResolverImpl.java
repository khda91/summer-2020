package ru.levelup.qa.at.design.patterns.strategy;

public class TaxResolverImpl implements TaxResolver {

    TaxCalculator calculator;

    public TaxResolverImpl(TaxCalculator calculator) {
        this.calculator = calculator;
    }

    @Override
    public double calculateTax(double sum) {
        return calculator.calculateTax(sum);
    }
}
