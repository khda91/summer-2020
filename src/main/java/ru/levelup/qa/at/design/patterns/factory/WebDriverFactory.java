package ru.levelup.qa.at.design.patterns.factory;

import io.github.bonigarcia.wdm.WebDriverManager;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.ie.InternetExplorerDriver;
import org.openqa.selenium.safari.SafariDriver;

public class WebDriverFactory {

    public static WebDriver createDriver(Browser browser) {
        switch (browser) {
            case IE: return createIE();
            case CHROME: return createChrome();
            case SAFARI: return createSafari();
            case FIREFOX: return createFirefox();
            default:
                throw new IllegalArgumentException(String.format("Unsupported browser '%s'", browser));
        }
    }

    private static WebDriver createFirefox() {
        WebDriverManager.firefoxdriver().setup();
        return new FirefoxDriver();
    }

    private static WebDriver createSafari() {
        return new SafariDriver();
    }

    private static WebDriver createChrome() {
        WebDriverManager.chromedriver().setup();
        return new ChromeDriver();
    }

    private static WebDriver createIE() {
        WebDriverManager.iedriver().setup();
        return new InternetExplorerDriver();
    }

}
