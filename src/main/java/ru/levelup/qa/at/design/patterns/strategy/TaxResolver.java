package ru.levelup.qa.at.design.patterns.strategy;

public interface TaxResolver {

    double calculateTax(double sum);
}
