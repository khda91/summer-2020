package ru.levelup.qa.at.design.patterns.singleton;

public class Singleton {

    private static Singleton instance;

    private final String value;

    private Singleton(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public static Singleton getInstance(String value) {
        if (instance == null) {
            System.out.println("Create singleton");
            instance = new Singleton(value);
        }
        return instance;
    }

}
