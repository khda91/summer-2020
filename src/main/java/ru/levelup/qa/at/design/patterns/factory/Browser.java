package ru.levelup.qa.at.design.patterns.factory;

public enum Browser {
    CHROME,
    FIREFOX,
    SAFARI,
    IE;
}
