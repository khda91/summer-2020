package ru.levelup.qa.at.design.patterns.builder;

public class Person {

    private String firstName;
    private String secondName;
    private String dateOfBirth;
    private int passportSeries;
    private int passportNumber;
    private String address;

    private Person(String firstName, String secondName, String dateOfBirth, int passportSeries, int passportNumber, String address) {
        this.firstName = firstName;
        this.secondName = secondName;
        this.dateOfBirth = dateOfBirth;
        this.passportSeries = passportSeries;
        this.passportNumber = passportNumber;
        this.address = address;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getSecondName() {
        return secondName;
    }

    public String getDateOfBirth() {
        return dateOfBirth;
    }

    public int getPassportSeries() {
        return passportSeries;
    }

    public int getPassportNumber() {
        return passportNumber;
    }

    public String getAddress() {
        return address;
    }

    public static Builder builder() {
        return new Builder();
    }

    @Override
    public String toString() {
        return "Person{" +
                "firstName='" + firstName + '\'' +
                ", secondName='" + secondName + '\'' +
                ", dateOfBirth='" + dateOfBirth + '\'' +
                ", passportSeries=" + passportSeries +
                ", passportNumber=" + passportNumber +
                ", address='" + address + '\'' +
                '}';
    }

    static class Builder {

        private String firstName;
        private String secondName;
        private String dateOfBirth;
        private int passportSeries;
        private int passportNumber;
        private String address;

        public Builder() {
        }

        public Builder firstName(String firstName) {
            this.firstName = firstName;
            return this;
        }

        public Builder secondName(String secondName) {
            this.secondName = secondName;
            return this;
        }

        public Builder dateOfBirth(String dateOfBirth) {
            this.dateOfBirth = dateOfBirth;
            return this;
        }

        public Builder passportSeries(int passportSeries) {
            this.passportSeries = passportSeries;
            return this;
        }

        public Builder passportNumber(int passportNumber) {
            this.passportNumber = passportNumber;
            return this;
        }

        public Builder address(String address) {
            this.address = address;
            return this;
        }

        public Person build() {
            return new Person(firstName, secondName, dateOfBirth, passportSeries, passportNumber, address);
        }
    }
}
