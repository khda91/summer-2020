package ru.levelup.qa.at.design.patterns.strategy;

public class GermanyTaxCalculator implements TaxCalculator {

    @Override
    public double calculateTax(double sum) {
        if (sum <= 5000) {
            return sum * 0.1;
        } else if (sum > 5000 && sum <= 15000) {
            return sum * 0.22;
        }
        return sum * 0.33;
    }
}
