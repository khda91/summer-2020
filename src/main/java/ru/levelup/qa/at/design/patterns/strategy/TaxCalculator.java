package ru.levelup.qa.at.design.patterns.strategy;

public interface TaxCalculator {

    double calculateTax(double sum);
}
