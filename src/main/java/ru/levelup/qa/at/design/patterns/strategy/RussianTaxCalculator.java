package ru.levelup.qa.at.design.patterns.strategy;

public class RussianTaxCalculator implements TaxCalculator {

    private static final double TAX_VALUE = 0.13;

    @Override
    public double calculateTax(double sum) {
        return sum * TAX_VALUE;
    }
}
