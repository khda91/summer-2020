package ru.levelup.qa.at.selenium.po.voids;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class LoginRegistrationPage {

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(name = "login")
    private WebElement usernameTextField;

    @FindBy(xpath = "//form[contains(@action, 'login')]//input[@name='password']")
    private WebElement passwordTextField;

    @FindBy(xpath = "//input[@value='Авторизоваться']")
    private WebElement authorizationButton;

    public LoginRegistrationPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    public void login(String username, String password) {
        wait.until(visibilityOf(usernameTextField)).sendKeys(username);
        wait.until(visibilityOf(passwordTextField)).sendKeys(password);
        wait.until(elementToBeClickable(authorizationButton)).click();
    }
}
