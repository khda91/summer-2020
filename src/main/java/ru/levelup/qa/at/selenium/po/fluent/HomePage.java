package ru.levelup.qa.at.selenium.po.fluent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class HomePage extends AbstractBasePage {

    private static final String URL = "http://users.bugred.ru/";

    @FindBy(xpath = "//a//span[text()='Войти']")
    private WebElement enterButton;

    @FindBy(className = "dropdown-toggle")
    private WebElement userButton;

    @FindBy(linkText = "Выход")
    private WebElement exitButton;

    public HomePage(WebDriver driver) {
        super(driver);
    }

    public HomePage open() {
        driver.get(URL);
        return this;
    }

    public LoginRegistrationPage clickEnterButton() {
        elementClick(enterButton);
        return new LoginRegistrationPage(driver);
    }

    public boolean isEnterButtonDisplayed() {
        return wait.until(elementToBeClickable(enterButton)).isDisplayed();
    }

    public HomePage clickUserButton() {
        elementClick(userButton);
        return this;
    }

    public String getTextUserButton() {
        return wait.until(visibilityOf(userButton)).getText();
    }

    public HomePage clickExitButton() {
        elementClick(exitButton);
        return this;
    }
}
