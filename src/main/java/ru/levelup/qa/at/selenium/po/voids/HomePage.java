package ru.levelup.qa.at.selenium.po.voids;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.refreshed;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class HomePage {

    private static final String URL = "http://users.bugred.ru/";

    private WebDriver driver;
    private WebDriverWait wait;

    @FindBy(xpath = "//a//span[text()='Войти']")
    private WebElement enterButton;

    @FindBy(className = "dropdown-toggle")
    private WebElement userButton;

    @FindBy(linkText = "Выход")
    private WebElement exitButton;

    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    public void open() {
        driver.get(URL);
    }

    public void clickEnterButton() {
        wait.until(elementToBeClickable(enterButton)).click();
    }

    public boolean isEnterButtonDisplayed() {
        return wait.until(elementToBeClickable(enterButton)).isDisplayed();
    }

    public void clickUserButton() {
        wait.until(elementToBeClickable(userButton)).click();
    }

    public String getTextUserButton() {
        return wait.until(visibilityOf(userButton)).getText();
    }

    public void clickExitButton() {
        wait.until(elementToBeClickable(exitButton)).click();
    }
}
