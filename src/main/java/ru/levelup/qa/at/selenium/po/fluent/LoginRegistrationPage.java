package ru.levelup.qa.at.selenium.po.fluent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public class LoginRegistrationPage extends AbstractBasePage {

    @FindBy(name = "login")
    private WebElement usernameTextField;

    @FindBy(xpath = "//form[contains(@action, 'login')]//input[@name='password']")
    private WebElement passwordTextField;

    @FindBy(xpath = "//input[@value='Авторизоваться']")
    private WebElement authorizationButton;

    public LoginRegistrationPage(WebDriver driver) {
        super(driver);
    }

    public HomePage successfulLogin(String username, String password) {
        login(username, password);
        return new HomePage(driver);
    }

    public LoginRegistrationPage wrongLogin(String username, String password) {
        login(username, password);
        return new LoginRegistrationPage(driver);
    }

    private void login(String username, String password) {
        sendKeysToElement(usernameTextField, username);
        sendKeysToElement(passwordTextField, password);
        elementClick(authorizationButton);
    }

    public boolean isAuthorizationButtonDisplayed() {
        return wait.until(elementToBeClickable(authorizationButton)).isDisplayed();
    }
}
