package ru.levelup.qa.at.selenium.po.fluent;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.WebDriverWait;

import static org.openqa.selenium.support.ui.ExpectedConditions.elementToBeClickable;
import static org.openqa.selenium.support.ui.ExpectedConditions.visibilityOf;

public abstract class AbstractBasePage {

    protected WebDriver driver;
    protected WebDriverWait wait;

    protected AbstractBasePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 15);
        PageFactory.initElements(driver, this);
    }

    protected void elementClick(WebElement element) {
        wait.until(elementToBeClickable(element)).click();
    }

    protected void sendKeysToElement(WebElement element, String text) {
        WebElement textField = wait.until(visibilityOf(element));
        textField.clear();
        textField.sendKeys(text);
    }
}
