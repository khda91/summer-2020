package ru.levelup.qa.at.allure;

import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;

public class LoginRegistrationPage {

    public static SelenideElement usernameTextField = $("[name='login']");

    public static SelenideElement passwordTextField = $x("//form[contains(@action, 'login')]//input[@name='password']");

    public static SelenideElement authorizationButton = $x("//input[@value='Авторизоваться']");

    @Step("Получить имя пользователя")
    public SelenideElement userButton() {
        return $(".dropdown-toggle");
    }

    @Step("Имя пользователя должно соответствовать '{0}'")
    public void userButtonShouldHaveText(String text) {
        $(".dropdown-toggle").shouldHave(text(text));
    }
}
