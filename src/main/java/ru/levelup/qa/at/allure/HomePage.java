package ru.levelup.qa.at.allure;

import com.codeborne.selenide.Selenide;
import com.codeborne.selenide.SelenideElement;
import io.qameta.allure.Step;

import static com.codeborne.selenide.Condition.text;
import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.$x;
import static com.codeborne.selenide.Selenide.page;

public class HomePage {

    private static final String URL = "http://users.bugred.ru/";

    @Step("Открывается домашняя страница 'http://users.bugred.ru/'")
    public HomePage open() {
        return Selenide.open(URL, HomePage.class);
    }

    @Step("Нажать на кнопку войти")
    public LoginRegistrationPage clickEnterButton() {
        $x("//a//span[text()='Войти']").click();
        return page(LoginRegistrationPage.class);
    }

//    public void clickEnterButton() {
//        $x("//a//span[text()='Войти']").click();
//    }

    @Step("Получить имя пользователя")
    public SelenideElement userButton() {
        return $(".dropdown-toggle");
    }

    @Step("Имя пользователя должно соответствовать '{0}'")
    public void userButtonShouldHaveText(String text) {
        $(".dropdown-toggle").shouldHave(text(text));
    }
}
